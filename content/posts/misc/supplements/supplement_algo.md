# Supplement for long covid

Gather the list of supplements for long covid from the following sources

- Research
- Social media

## Supplements from Research

- Read research papers about nutrients and minerals that might be deficient post covid.
- Ongoing and planned trials of supplements for covid/long covid
- Ongoing treatment protocols

## Supplements from Social media

- Gather what long haulers are taking from Twitter
- Reach out to recovered people to know what supplement possibly worked
- BodyPolitic slack channels
- long covid subreddit

- Finally once you have all the above data, reach out to the doctor/nutritionist who can help with the supplements.
