# L-arginine to hospitalized patients

## Study

### Goal

> L-arginine has been shown to improve endothelial dysfunction.

Study the above in the context of acute covid

### RCT

- Patients received 1.66 g L-arginine twice a day or placebo, administered orally.

> The primary efficacy endpoint was a reduction in respiratory support assessed 10 and 20 days after randomization. Secondary outcomes were the length of in-hospital stay, the time to normalization of lymphocyte number, and the time to obtain a negative real-time reverse transcription polymerase chain reaction (RT-PCR) for SARS-CoV-2 on nasopharyngeal swab.

Presence of all the following conditions:

- Pneumonia confirmed by chest imaging;
- Oxygen saturation of `93%` or lower on room air;
- Ratio of alveolar oxygen partial pressure to fractional inspired oxygen (PaO2/FiO2) of 300 or less;
- Lymphocytopenia, defined as lymphocytes `< 1500/μL or < 20%` of white blood cells.

### Observations

> Strikingly, patients treated with L-arginine exhibited a significantly reduced in-hospital stay vs placebo.

### My takeaways

- Large RCT required
- This study does not discuss if the L-arginine patients went on to develop long covid.

## Blog post notes

L-arginine is required to make Nitric oxide(NO). Also arginine si requried for lymphocytes to proliferate.

### NO roles

- antimicrobial and helps to kill pathogens
- vasodilator and prevents Platelet adhesion
- In lungs, NO combines with glutathione to make nitrosoglutathione which is a broncodilator(opens up the airways).

---

## References

- [Effects of adding L-arginine orally to standard therapy in patients with COVID-19: A randomized, double-blind, placebo-controlled, parallel-group trial. Results of the first interim analysis](<https://www.thelancet.com/journals/eclinm/article/PIIS2589-5370(21)00405-3/fulltext>)
- [The One Amino Acid That Could Cure COVID](https://chrismasterjohnphd.com/blog/2021/10/01/the-one-amino-acid-that-cured-covid)
