# Amino acids, natural extracts for Long covid

Given so many studies point to vascular side of long covid from endothelial dysfunction, microclots, immune dysfunction, I would like to explore supplements that can help on the vascular side.

I am looking at the following BCAA, NAC, L-arginine, acetyl l-carnitine/l-carnitine, coq10, curcumin

## Cooperative Effects of Q10, Vitamin D3, and L-Arginine on Cardiac and Endothelial Cells `[1]`

> In cardiac and endothelial cells, Q10, L-arginine, and vitamin D3 combined were able to induce a nitric oxide production higher than the that induced by the 3 substances alone.
> The effects on vasodilation induced by cooperative stimulation have been confirmed in an in vivo model as well.
> The use of a combination of Q10, L-arginine, and vitamin D to counteract increased free radical production could be a potential method to reduce myocardial injury or the effects of aging on the heart.

## Can l-carnitine reduce post-COVID-19 fatigue?

This is not a invivo study.

- L-carnitine is an amino acid that plays important role in metabolism of fatty acids. Structurally similar to choline. This hydrophilic amino acid is existent all through the CNS and PNS and is found mainly in the heart and skeletal muscles.
- Can prevent muscle-wasting, anti-apoptic, anti-inflammatory, antioxidant properties, interfere with acetylcholine synthesis in the brain.

> l-Carnitine is biosynthesized from the amino acids lysine and methionine in the kidneys, liver, and brain and in a process that requires vitamins B6, B3, C, niacin, and iron.
> Impaired synthesis, transport, or metabolism of l-carnitine can lead to early or secondary deficiencies; this can lead to increased intracellular lipid levels in muscles.
> **Suggested mechanisms for fatigue** include imbalances in energy production caused by increased energy requirement (e.g., caused by tumor growth, infection, fever, or surgical procedures), reduced availability of substrates (e.g., due to anorexia, nausea, or vomiting), atypical generation of compounds that disrupt metabolic homeostasis or regular functions of muscles (e.g., cytokines and proteolysis inducers), anemia, and **hypoxemia**.
> Proinflammatory cytokines such as IL-1b, IL-6, and TNF-a induce fever, fatigue, anorexia, and cognitive dysfunction.
> Decreased serum acetyl-l-carnitine, polymorphism of serotonin genes, and **autoantibodies against [muscarinic cholinergic receptors(GPCR receptor)][2]** were seen in cases experiencing CFS.
> An increase in concentration of serum l-carnitine (through supplementation) can increment l-carnitine transportation through skeletal muscles and neuromuscular junctions; this efficacy may reduce hypoxia and induce acetylcholine synthesis.
> l-Carnitine can be beneficial against the antioxidant effects of angiotensin II by inhibiting NF-κB and down-regulating NOX1 and NOX2.
> Post-infection fatigue after Epstein Barr virus infection has been evidently supported by alteration in the expression of genes associated with mitochondrial function such as fatty acid metabolism and apoptosis

Alteration at the gene level ?????? [It seems to happen port viral infection][1]

### My takeaways

- Raw material deficiency (Vit C, B3, B6, niacin, iron)
- Impaired synthesis due to factors like hypoxemia or blood flow(endothelial dysfunction) or organ damage.
- Even with proper synthesis, autoantibodies(ex: GPCR) to the receptors make its bioavailability useless. Note that **Acetylcholine is the primary neurotransmitter of the parasympathetic nervous system**.
- If there is an alteration in the gene expression associated with mitochondrial function, then it basically saying I am mutated to be a sloth at the mitochondrial level?

### Assessing the antioxidant and metabolic effect of an alpha-lipoic acid and acetyl-L-carnitine nutraceutical

This is both invitro and invivo(but very small)

> These bioactive compounds have a high impact in the regulation of oxidative stress and in the improvement of the mitochondrial function, organelle where they exert their main action
> ALC facilitates the transport of long-chain fatty acids from the cytosol into the mitochondria, where their degradation takes place via β-oxidation

---

## References

1. [Cooperative Effects of Q10, Vitamin D3, and L-Arginine on Cardiac and Endothelial Cells](https://www.karger.com/Article/Pdf/484928)
2. [Can l-carnitine reduce post-COVID-19 fatigue?](https://www.sciencedirect.com/science/article/pii/S2049080121010955)
3. [Assessing the antioxidant and metabolic effect of an alpha-lipoic acid and acetyl-L-carnitine nutraceutical](https://www.sciencedirect.com/science/article/pii/S2665927121000356)

[1]: https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC2716361/
[2]: https://www.ncbi.nlm.nih.gov/books/NBK526134/
