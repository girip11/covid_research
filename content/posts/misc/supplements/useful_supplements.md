# List of useful supplements for longcovid

Based on the trials for covid, I am curating the list of supplements that might be useful. Its just a curated list of trialled supplements and not a recommendation. As a long covid patient you might not require all of the below supplements. Before taking these supplements just check with your doctor on any existing condition acting as contraindicator.

## B vitamins

Most long haulers tend to have vitamin B deficiency.

- B complex with all B vitamins

If you have high homocysteine levels due to B12 and/or folate deficiency, then B12 and B9 supplements will help

- Vitamin B12 `[11, 13]` - **1500mcg/day**
- Vitamin B9 (folic acid) `[12, 13]` - **0.-1.5mg/per day**

## Iron deficiency Anemia

- Iron `[14]`

## Immune system

- Vitamin C `[1, 3, 4, 5]` - **500-1000mg/day**
- Zinc `[3, 4, 5, 20]` - **10-50mg/day**
- Selenium `[5, 20]` - **18-56mcg/day**

## Anti-inflammatory

- Quercetin `[1, 20]` - **100-350mg/day**
- Resveratol `[20]` - **100-250mg/day**
- Serrapeptase `[22]`

## Heart health

- Vitamin E - **200-400mg/day**
- Omega-3 fatty acids (EPA/DHA) `[1,4,5]`

## Bone health

- Magnesium `[4]` - **300-500mg/day**
- Vitamin K2(MK-7)`[2]` - **90-110mcg/day**
- Calcium - **500mg/day**
- Vitamin D `[1, 3, 4, 5]` - **2000-4000IU** per day. Refer [this resource][1] for dosage details

## Metabolism

- BCAA (Leucine, Isoleucine, Valine) `[6]`
- L-arginine `[6, 8]` - **1-2g/day**

## Cognitive health

- Acetyl-l-carnitine `[18]` - **500mg/day**
- Citicoline - **500mg/day**
- Piracetam - **800mg/day**

## Mitochondrial support

- L-carnitine `[18, 21]` - **500mg** once/twice per day
- CoQ10 `[9, 21]`- **200mg/day**
- Niacin flush or NADH supplementation `[19, 20]`.

## Antioxidants

- Alpha lipoic acid `[15, 16, 17]` - **300mg/day**
- Glutamine `[6]`
- Curcumin `[1]` - **500mg** 1-2 times per day

## Respiratory

- N-acetyl cysteine`[6, 7, 15]` - **600mg** once/twice per day

## Gut

- Probiotic (with Bifidobacterium strains) once/twice a day `[1, 10]`

## Fibrinolysis

- Nattokinase - Fibrinolysis and anticoagulant. **2000-4000FU/day**
- Lumbrokinease - Fibrinolysis and anticoagulant.

## References

1. [I-RECOVER Management Protocol for Long Haul COVID-19 Syndrome (LHCS)][1]
2. [Randomized Controlled Clinical Trial to Investigate Effects of Vitamin K2 in COVID-19 (KOVIT)][2]
3. [Dietary Supplements for COVID-19][3]
4. [COVID-19 and nutritional deficiency: a review of existing knowledge][4]
5. [Immune-boosting role of vitamins D, C, E, zinc, selenium and omega-3 fatty acids: Could they help against COVID-19][5]
6. [Efficacy, Safety, Tolerability of AXA1125 in Fatigue Predominant PASC][6]
7. [N-Acetylcysteine to Combat COVID-19: An Evidence Review][7]
8. [Effects of adding L-arginine orally to standard therapy in patients with COVID-19: A randomized, double-blind, placebo-controlled, parallel-group trial. Results of the first interim analysis][8]
9. [Coenzyme Q10 as Treatment for Long Term COVID-19 (QVID)][9]
10. [Study shows probiotics can reduce symptoms of COVID-19 when taken post-exposure][10]
11. [Vitamin B12 Deficiency in COVID-19 Recovered Patients: Case Report][11]
12. [COVID-19: A methyl-group assault?][12]
13. [Nutritional status of patients with COVID-19][13]
14. [Persisting alterations of iron homeostasis in COVID-19 are associated with non-resolving lung pathologies and poor patients’ performance: a prospective observational cohort study][14]
15. [Three novel prevention, diagnostic, and treatment options for COVID-19 urgently necessitating controlled randomized trials][15]
16. [Alpha lipoic acid as a potential treatment for COVID-19 - a hypothesis][16]
17. [Alpha-lipoic acid may protect patients with diabetes against COVID-19 infection][17]
18. [Can l-carnitine reduce post-COVID-19 fatigue?][18]
19. [NAD+ deficiency][19]
20. [Dr. Ade Wentzel, Robert Miller and Guy Richards Protocol][20]
21. [Mitochondrial Dysfunction and Chronic Disease: Treatment With Natural Supplements][21]
22. [The role of serratiopeptidase in the resolution of inflammation][22]
23. [Effect of Citicoline on Memory Function in Healthy Order Adults: A Randomized, Double-Blind, Placebo-Controlled Clinical Trial][23]
24. [Piracetam and piracetam-like drugs: from basic science to novel clinical applications to CNS disorders][24]

[1]: https://covid19criticalcare.com/covid-19-protocols/i-recover-protocol/
[2]: https://clinicaltrials.gov/ct2/show/NCT04770740
[3]: https://clinicaltrials.gov/ct2/show/NCT04780061
[4]: https://pubmed.ncbi.nlm.nih.gov/33544528/
[5]: https://pubmed.ncbi.nlm.nih.gov/33308613/
[6]: https://www.clinicaltrials.gov/ct2/show/NCT05152849
[7]: https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC7649937/
[8]: https://www.thelancet.com/journals/eclinm/article/PIIS2589-5370(21)00405-3/fulltext
[9]: https://clinicaltrials.gov/ct2/show/NCT04960215
[10]: https://www.news-medical.net/news/20220107/Study-shows-probiotics-can-reduce-symptoms-of-COVID-19-when-taken-post-exposure.aspx
[11]: https://pesquisa.bvsalud.org/global-literature-on-novel-coronavirus-2019-ncov/resource/en/covidwho-1022367
[12]: https://www.sciencedirect.com/science/article/pii/S030698772100061X
[13]: https://www.sciencedirect.com/science/article/pii/S1201971220306470
[14]: https://respiratory-research.biomedcentral.com/articles/10.1186/s12931-020-01546-2
[15]: https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC7242962/
[16]: https://pesquisa.bvsalud.org/global-literature-on-novel-coronavirus-2019-ncov/resource/en/covidwho-1050862
[17]: https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC7427590/
[18]: https://www.sciencedirect.com/science/article/pii/S2049080121010955
[19]: https://www.sciencedirect.com/science/article/pii/S0306987720314742
[20]: https://famedomain.com/covid-scientists-dr-ade-wentzel-robert-miller-and-guy-richards/
[21]: https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC4566449/
[22]: https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC7032259/
[23]: https://academic.oup.com/cdn/article/4/Supplement_2/1227/5845231
[24]: https://pubmed.ncbi.nlm.nih.gov/20166767/
