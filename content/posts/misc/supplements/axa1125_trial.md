# AXA1125 for long covid

- Targeting fatigue and mitochondrial dysfunction

## AXA1125 drug composition

- Leucine - 1g
- Isoleucine - 0.5g
- valine - 0.5g
- arginine - 1.81g
- glutamine - 2g
- N-AcetylCysteine - 150mg

## Goals

The improvement of mitochondrial function in the skeletal muscle from baseline to day 28 as assessed by variations in phosphocreatine (PCr) recovery time will be the trial’s primary goal.

Lactate levels, fatigue scores, a walk test for six minutes, and safety and tolerability will be the key secondary goals.

---

## References

- [Oxford to test potential treatment for fatigue in long COVID patients](https://www.ox.ac.uk/news/2021-10-29-oxford-test-potential-treatment-fatigue-long-covid-patients)
- [Axcella commences trial of AXA1125 for long Covid-19 treatment](https://www.clinicaltrialsarena.com/news/axcella-trial-long-covid-treatment/)
