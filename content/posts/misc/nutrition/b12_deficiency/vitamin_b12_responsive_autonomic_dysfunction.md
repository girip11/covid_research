# Vitamin B12-responsive severe leukoencephalopathy and autonomic dysfunction in a patient with “normal” serum B12 levels

## Case study

- Depression, cognitive decline, progressive leukoencephalopathy, autonomic dysfunction.
- Patient with serum B12 levels in the normal range, homocysteine and methylmalonic acid levels normal too.
- Tested positive for intrinsic factor antibodies - indicative of pernicious anemia.
- Treated with Cyanocobalamin `1000mcg` injections daily for 1 week, weekly once for 6 weeks and monthly once alongside vitamin D supplementation.
- Patient remarkable recovery in 3 months. Able to walk and normal cognition.

## Patient history

- Developed depression. Treated with antidepressants
- Developed frequent syncopal episodes.
- Felt dizzy in the standing position.
- Autonomic testing revealed sympathetic and parasympathetic failure.
- Blood pressure dropped without significant changes in heart rate variability.
- Progressed to severe orthostatic intolerance. Salt supplementation, midodrine, fludrocortisones had minimal impact.
- Developed weight loss
- Husband notices cognition worsening with memory and concentration affected.
- Extensive blood work done next 4 years. Serum B12 levels ranged from 267-447 and homocysteine and MMA levels were normal.
- Brain MRIs revealed whitematter T2 signal h yperintensities and progressive brain atrophy.
- Tested for intrinsic factor antibody and it was positive.
- MRI post treatment showed significant resolution of the white matter abnormalities.

## Notes

> Pathologically, subacute combined degeneration shows myelinolysis and astrogliosis without oligodendroglial involvement, and resulting interstitial oedema and myelin sheath swelling without inflammatory infiltrates. The reversibility of even long-standing neurologic dysfunction is possible based on the dysmyelinating nature of neurologic B12
>
> Testing for anti-intrinsic factor or anti-parietal cell antibodies may provide evidence of pernicious anaemia and gastritis.
> However, despite recovery of our patient’s symptoms and white matter abnormalities, MRI showed progressive ventricular dilatation and cerebral atrophy. Longstanding B12 and/or vitamin D deficiency may have contributed to this because B12 levels have been associated with brain volume loss.

## References

- [Vitamin B12-responsive severe leukoencephalopathy and autonomic dysfunction in a patient with “normal” serum B12 levels](https://pubmed.ncbi.nlm.nih.gov/20587489/)
