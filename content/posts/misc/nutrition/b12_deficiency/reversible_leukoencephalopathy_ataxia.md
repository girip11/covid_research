# Vitamin B12 deficiency manifesting as reversible leukoencephalopathy and ataxia

## Terminologies

- Ataxia - Coordination, balance and speech affected. Exhibit difficulties with balance, walking, speaking and swallowing.

## Case report

- 52 male, vegetarian diet with no comorbidities.
- Rapidly progressive cognitive decline (4 months). Initial behavioural changes noted are aggressiveness, delusion, disorganized thought process and apathy.
- Gradual worsening of imbalance while walking
- Incontinence of urine and stools
- Haemoglobin was 9.6 and peripheral smear showed macrocytes and anisocytosis.
- Low serum B12 levels. Plasma homocysteine level was elevated.
- Other blood tests were normal and MRI brain revealed with white matter hyperintensities.
- PET scan showed hypometabolism.

> Patient was started on intramuscular injections of methylcobalamin 1000 µgm daily for 7 days followed by weekly injections for a month, followed once a month for 6 months and 6 monthly thereafter.

- Follow up imaging 18 months later showed regression of signal changes in the brain white matter and resolution of posterious column signal changes in the spinal cord.

---

## References

- [Vitamin B12 deficiency manifesting as reversible leukoencephalopathy and ataxia](<https://www.neurology-asia.org/articles/neuroasia-2021-26(1)-183.pdf>)
