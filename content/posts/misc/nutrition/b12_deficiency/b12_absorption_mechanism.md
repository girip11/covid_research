# The Mechanism of Absorption of Vitamin B12 (cobalamin) in the GI Tract

- B12(aka cobalamin) deficiency can cause spinary cord related neuropathyies, hematopoietic (RBC) disorders.

![B12 absorption](vitb12_absorption.png)

## B12 ingested in protein bound form

- B12 will be bound to protein in food sources(meat).
- Proteolytic cleavage in the stoamch or duodenum
- Then B12 will bind to R-binder and then enters duodenum.
- Stomach cells produce pepsinogen and the parietal cells secrete HCL which converts pepsinogen to pepsin. Functionally active pepsin helps in separating the protein bound B12 and make it available in the free form.

## B12 in free form

- Vitamin B12 ingested in free form will bind to R-binder(carrier protein) called as **transcobalamin I**.
- This carrier protein is secreted by both salivary glands and the gastric mucosal cells in teh stomach.
- This complex(B12 and R-Binder) reaches the duodenum in the small intestine.

## Inside Duodenum

- Parietal cells(if properly functioning) will also secrete intrinsic factor which should also reach duodenum

- Duodenum also has the B12/R-binder complex.

- When the B12-R-binder complex reaches the second segment of duodenum, pancrease will release additional protease to free the B12 from the R-binder complex. At this point the B12 will bind to the intrinsic factor and will travel to the ileum of the small intestine for absorption.

- With a functionally intact ileum, B12/intrinsic factor complex is absorbed and then it binds to **transcobalamin II**. 50% of this is delivered to the liver and remainder to the tissues. Liver can store significant amount of B12.

## Key takeaways

- Salivary glands and gastric mucosal cells have to be working to secrete R-binders
- Parietal cells must work and should release intrinsic factor and this intrinsic factor should not be neutralized by antibodies.
- If we have low stomach acid, then absorption from meat becomes a challenge(pepsinogen to pepsin is affected).
- Pancreas should function properly to release the protease responsible for spliting the R-binder and B12.
- Intrinsic factor and B12 complex needs to be absorbed by the ileum. Small intestine ileum has to be intact.
- Liver B12 store should be functioning well.

---

## References

- [The Mechanism of Absorption of Vitamin B12 (cobalamin) in the GI Tract](https://www.ebmconsult.com/articles/vitamin-b12-absorption-mechanism-intestine-intrinsic-factor)
