# Vitamins Overview

## Vitamin B1

[RDA 1.2 mg][4] daily. Dietary sources include

- Fortified cereals
- Beans
- Lentils
- Green peas
- Sunflower seeds
- Yogurt

## Vitamin B2

[RDA 1.3 mg][3] daily. Dietary sources include

- Dairy products
- Yogurt
- Cheese
- Eggs
- Fortified cereals
- Almonds
- Spinach

## Vitamin B6

[RDA 1.3 mg][2] daily. Dietary sources include

- Milk
- Ricotta Cheese
- Eggs
- Carrot
- Spinach
- Potato
- Sweet Potato
- Green peas
- Chickpeas
- Fortified breakfast cereals
- Avocado
- Bananas
- Papayas
- Oranges
- Dates

---

## References

- [B vitamins and folic acid](https://www.nhs.uk/conditions/vitamins-and-minerals/vitamin-b/)

[1]: https://www.healthline.com/health/vitamin-b6-foods
[2]: https://www.hsph.harvard.edu/nutritionsource/vitamin-b6/
[3]: https://www.hsph.harvard.edu/nutritionsource/riboflavin-vitamin-b2/
[4]: https://www.hsph.harvard.edu/nutritionsource/vitamin-b1/
