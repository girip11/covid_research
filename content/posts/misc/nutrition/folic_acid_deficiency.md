# Folic acid deficiency

- Folate - water soluble vitamin
- Present in fruits, green leafy vegetables, citrus fruits and meat
- Supplements and fortified foods contain folic acid which is the synthesized form of folate.

## Causes

- Inadequate from dietary sources. Heating destroys folic acid.
- Folate absorbed in the jejunum across the intestinal wall. So celiac disease, amyloidosis, mesenteric vascular insufficiency can inhibit folate absorption.
- Certain drugs like methotrecate can also antogonize the folate utilization
- Folic acid deficiency can occur subsequent to B12 deficiency.
- Alcoholism
- Pregnancy
- Hemolytic anemia
- Dialysis

**NOTE**: Overconsuming folate increases the risk of developing tumours.

## Pathophysiology

- Serum folate is present in inactive 5-methly-tetrahydrofolate(5-methly-THFA) form.
- `5-methyl THFA -> Cobalamin(cofactor) -> THFA`
- From serum, upon entering the cells it demethylates to THFA which is biologically active form involved in folate dependent enzymatic reactions
- Cobalamin (B-12) as a cofactor in the demethylation. So B12 deficiency also affects folate dependent enzymatic reactions.

> THFA is involved in the formation of many coenzymes in metabolic systems, particularly for purine and pyrimidine synthesis, nucleoprotein synthesis, and maintenance in erythropoiesis.

- Folate deficiency may take 8-16 weeks to become evident.
- Deficiency leads to impairment of cell division, accumulation of toxic metabilites, impartment of methylation reactions required for regulation of gene expression.

- Body has 1000-2000mcg of folate store. RDA is 400mcg.

## Symptoms

- Painful beefy red tongue
- Anorexia - Loss of appetite
- Cognitive impairment, depression and dementia

## Diagnosis

- CBC test
- Anemia with decrease in haemoglobin and hematocrit levels.
- MCV > 100
- Serum vitamin B12 and folate levels are also used.
- Serum folate `<2ng/ml` deficient
- Folate deficiency can be confirmed with a normal B12 and MMA level and elevated homocysteine levels
- B12 deficiency can be confirmed with elevated MMA and homocysteine levels and low B12 levels.

## Treatment

- Folic acid 1-5mg daily

---

## References

- [Folic acid deficiency](https://www.ncbi.nlm.nih.gov/books/NBK535377/)
- [Folic acid](https://patient.info/medicine/folic-acid-folate)
