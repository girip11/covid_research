# The Vitamin-D Wellness Protocol

Protocol involves supplementing with vitamin-D3, magnesium and vitamin-k2.

**NOTE** - Consult physician in case of liver or kidney issues prior to supplementing

## Vitamin D3

- Vitamin-D3 oil based soft gel supplements because most diets don't contain adequate Vitamin-D3.
- Preferably after breakfast
- Start with 5000IU per day and increase to 10000IU per day after a week.
- Direct sun exposure every day 15-20 minutes.

## Vitamin-K2

- Dairy products are a source of Vitamin-K2.
- Recommended dosage is 100mcg of Vitamin-K2(MK-7) daily
- Preferably after breakfast along with the Vitamin-D3 supplement.
- If you are on blood thinning medication, then this supplement (or any form of Vitamin-K) should not be taken without consulting the physician.

## Magnesium

- Leafy green vegetables, legumes, nuts and seeds are rich in Magnesium.
- Magnesium Glycinate preferred form.
- 400 - 600 mg daily before bedtime. It helps with sleep too.
- Those taking thyroid medication should wait at least 4 hours before supplementing with any minerals like Magnesium.

## Calcium

- Dietary sources include leafy green vegetables, dairy products, nuts, legumes and seeds.
- Only supplement with Calcium when the **Serum calcium** level is below normal. Otherwise, dietary sources are good enough.

**NOTE** - When in doubt, review the list of supplements with your healthcare practitioner.

---

## References

- [The Vitamin-D Wellness Protocol](https://smilinsuepubs.com/2015/03/06/vitamin-d-wellness-protocol)
