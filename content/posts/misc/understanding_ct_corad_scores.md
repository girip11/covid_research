# Understanding CT Scan scoring

## CO-RADS

For CO-RADS the level of suspicion of COVID-19 infection was graded from very low (CO-RADS 1) up to very high (CO-RADS 5).

## CT IS

The CTIS was based on the extent of lobar involvement of typical COVID-19 CT findings. This extent was evaluated by scoring the percentages of involvement in each of the five lobes (CTIS 1 for <5% involvement, score 2 for 5–25% involvement, score 3 for 26–50% involvement, score 4 for 51–75% involvement, and score 5 for >75% involvement), resulting in a total score on **25**.

Three CTIS-based categories were defined as: 0–2, 3–6, >6 representing respectively mild, moderate, and severe lung involvement.

## CT-SS (Semiquantitative Scoring)

The CT-SS was calculated based on the extent of lobar involvement. Each of the five lung lobes was visually scored on a scale of 0–5, with 0 indicating no involvement, 1 indicating less than 5% involvement, 2 indicating 5–25% involvement, 3 indicating 26–49% involvement, 4 indicating 50–75% involvement, and 5 indicating more than 75% involvement. The total CT score was the sum of the individual lobar scores and ranged from 0 (no involvement) to 25 (maximum involvement

---

## References

- [The mean severity score and its correlation with common computed tomography chest manifestations in Egyptian patients with COVID-2019 pneumonia](https://ejrnm.springeropen.com/articles/10.1186/s43055-020-00368-y)
