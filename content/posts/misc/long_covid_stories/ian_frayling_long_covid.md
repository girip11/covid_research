# Long Covid journey of Dr. Ian Frayling

## Acute Covid Symptoms

- Loss of smell/taste
- Loss of appetite
- Severe bone-cracking fever
- Musculoskeletal pain
- Cough

## LC Symptoms

- Brain Fog
- Irritable bowel syndrome
- Nausea
- Disruptive sleep
- Difficulty with coordination
- Fatigue
- Feeling of low blood sugar levels

## Highlights

> - After actively choosing to avoid a hospital admission, which he now admits was a mistake in hindsight, Dr Frayling said he continued to have "waves" of illness from the virus.
> - I seem to have a cyclical version of chronic fatigue syndrome, which has given me a greater appreciation for people who have that condition.
> - I had severe clotting in the big vein down the centre of my brain, in the two big veins either side and clotting extending down into the big vein that goes into my neck.
> - Dr Frayling also suffered a damaged liver and pancreas, an enlarged spleen and lost about a third of his right lung function

## Key findings

- Problems with heart and blood pressure
- Clots in the brain!!!

Prescribed Warfarin. Felt better post anticoagulant cognitively. Clots disappeared in subsequent follow up examination.

---

## References

- [Coronavirus has left me with devastating after effects](https://www.walesonline.co.uk/news/health/doctor-believes-hes-suffering-long-19022875)
- [Doctor with long Covid found to have multiple blood clots in his brain](https://www.walesonline.co.uk/news/health/doctor-long-covid-found-multiple-22277494)
