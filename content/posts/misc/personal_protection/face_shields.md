# Face shields protection

[Face shields quick guide](face_shields_quick_guide.pdf)

## [Face shield protection and vortex rings][1]

- This study analyzed if only face shields can protect from the sneezes of other people in the proximity.

> When fluids (liquids and gases) are fired at high velocity through a circular aperture such as the mouth or a nostril, swirling vortex rings can be created.

- Sneezes create something called vortex rings. These rings can attract particles from the surrounding space.
- Simulation - Person wearing a face shield exposed to a sneeze from another person 1m apart.
- When the vortex rings reach the face shield, they can reach the top and the bottom of the shield. When this coincides with people inhaling, there is a possiblity of the particles carried by the vortex rings reaching the nose of the person wearing the shield.
- So the recommendation is to wear an additional protection(face mask) along with a face shield.

## [Face shields along with surgical masks][2]

- Simulation to test aerosol transmission protection using surgical mask, face shield and combination of both.
- Transmitted aerosols detected at 0.15 and 1.8 meters from the source.
- Surgical masks alone provided good protection while only face shields didnot. Face shields combined with the surgical mask offered similar(sligthly better but not a significant diff) protection to that of only surgical masks.

## [Face shields fail to give high levels of COVID protection: study][3]

- 13 different face shields were tested in a coughing simulation.
- All designs failed to offer complete protection, though they offered some level of protection.

> The shields that offered most protection were closed across the forehead and extended well around the sides of the face and below the chin.
> It's important to know that the lab experiments are in the scenario of someone actively coughing at the shield wearer from close proximity. But the chances of droplets getting around the shield onto the face from just speaking are much lower - [Face shields covid protection][4]

**NOTE** - This study considered only wearing a face shield.

## [Experimental Efficacy of the Face Shield and the Mask against Emitted and Potentially Received Particles][5]

- Study the effectiveness of face shields for source control.
- Both emitter and receiver wearing surgical masks as well as face shields
- Emitter and receiver manikins were face to face 25 cm apart with particle emission for 30 seconds.

> We used a liquid aerosol generator mimicking the particles emitted by the voice at room temperature, with a right-angle position close to the chin down, a chin and forehead overhang, and most importantly, a position of the visor parallel to the face.

- Receiver alone wearing a face shield was more effective compared to mask.
- Wearing of face shield/mask by the emitter reduced the level of received particles by 96.8%.
- Both emitter and receiver wearing a face shield, produced better results as per the experimental conditions. Face shields offered better barrier protection compared to masks in all configurations(98% vs 97.3%).

- This study finds that face shields are helpful with the short term exposure in preventing high exposure.

- This study also points our the [limitations with another study that discusses face shield for source control][11]

> Particles inhalation by aerosolization could also be achieved by long, low-intensity exposure: this is not tested here in our study.

- Face shields are recommended for short exposure situation.

## [Efficacy of Face Shields Against Cough Aerosol Droplets from a Cough Simulator][6]

- Examine the efficacy of face shields in reducing the exposure to the cough aerosol droplets.

**NOTE** - The current HICPAC guidelines explicitly recommend wearing a face shield or goggles during all patient care for certain illnesses such as severe acute respiratory syndrome (SARS) and avian influenza.

### Experiment

- Coughing patient Simulator and a breathing worker simulator to investigate the exposure.

- For the experiments performed with the aerosol particle measurement instruments, the parameters studied were the presence or absence of a face shield, the distance between the simulators (46 cm or 183 cm), and the cough aerosol particle size distribution (large or small).
- The experiments performed with influenza virus had the same experimental parameters, with the addition of the collection time (5 minutes or 30 minutes).

### Observations

- 0.9% of the initial burst of aerosol from a cough can be inhaled by a worker 46cm from the patient.

- Influenza laden cough aerosol was used in the experiment.

| Cough aerosol droplet diameter | Period       | Inhalation  | Mask Contamination |
| ------------------------------ | ------------ | ----------- | ------------------ |
| 8.5 micrometer                 | Immediate    | down by 96% | down by 97%        |
| 3.4 micrometer                 | Immediate    | blocked 68% | down by 76%        |
| finer aerosols                 | 1-30 minutes | blocked 23% | -                  |

- Increase the distance between patient and worker by 183cm, reduced the exposure after an immediate cough by 92%.

### Conclusion

- Face shields can reduce the short term exposure to large infectitious particles. But doesnt offer airborne protection.
- Face shields provide useful adjunct to respiratory protection.

### Detailed notes

- Using a numerical model, Xie et al. calculated that droplets as large as 200 μm could travel 1.5 m from a coughing patient before settling.
- Bischoff et al. collected aerosol samples in hospital rooms with influenza patients; they found that influenza-laden particles with diameters greater than 4.7 μm could be found 6 feet away from the patient, but that the amount of virus detected was much lower than at closer distances.

## [Efficacy of Various Facial Protective Equipment for Infection Control in a Healthcare Setting][7]

- Different face shields designs were tested and a basic mask was also tested.

> We found that the LCG shield was the most protective of the face shields tested.
>
> The facial PPE that had a shield – surgical mask, Medline, PRUSA, and LCG – were also shown to offer significantly more protection compared to the basic mask. When the PPE with shields were compared against each other, we found no statistically significant difference in the protection offered between the surgical mask, the PRUSA face shield (which can be manufactured with a three-dimensional printer [Prusa Research, Czechoslovakia]), and the Medline face shield (Medline Industries, Inc., Northfield, IL). The LCG shield, however, was shown to be statistically more protective than all other forms of facial PPE tested in this study.

NOTE : This study discusses about the droplet protection only.

> Limitations of this study include limited statistical power, imperfect cough simulation, and difficulty detecting the smallest aerosol particles.

## [SARS-CoV-2 Infection Among Community Health Workers in India Before and After Use of Face Shields][8]

- Observational study
- Study reports after adding face shield to the PPE, the workers who visited the homes (including those which had people testing positive) did not test positive.

> The face shields may have reduced ocular exposure or contamination of masks or hands or may have diverted movement of air around the face.
> Further investigation of face shields in community settings is warranted.

## [Investigation of the protection efficacy of face shields against aerosol cough droplets][10]

- Investigate face shields as a substitution for surgical masks using cough simulator.
- Droplet diameter more than 3 micrometer, face shield and surgical mask efficiency were comparable.
- Finer particles down to 0.3 micrometer, shield blocked 10 times more finer particles than that of a surgical mask.
- Side exposure depends on the geometry. Wider face shield provided good performance.

**NOTE** - Limitation of this study is that the protection was measured after a small time period after the cough exposure and the experiments were not conducted after longer periods of exposure. So it really doesnt convey much about aerosol protection.

## [Face Coverings, Aerosol Dispersion and Mitigation of Virus Transmission Risk][12]

[1]: https://physicsworld.com/a/face-shields-cannot-protect-wearers-from-virus-particles-carried-by-vortex-rings/
[2]: https://www.sciencedirect.com/science/article/abs/pii/S0196655321000213
[3]: https://www.ctvnews.ca/health/coronavirus/face-shields-fail-to-give-high-levels-of-covid-protection-study-1.6008314
[4]: https://www.sciencedaily.com/releases/2022/07/220727211929.htm
[5]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7922468/
[6]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4734356/
[7]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8463064/
[8]: https://jamanetwork.com/journals/jama/fullarticle/2769693
[9]: https://www.cdc.gov/hicpac/recommendations/core-practices.html
[10]: https://pubmed.ncbi.nlm.nih.gov/33315526/
[11]: https://pubmed.ncbi.nlm.nih.gov/32952381/
[12]: https://ieeexplore.ieee.org/abstract/document/9329130
