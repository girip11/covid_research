# Face mask fit modifications that improve source control performance

---

## References

- [Face mask fit modifications that improve source control performance](<https://www.ajicjournal.org/article/S0196-6553(21)00715-X/fulltext>)
