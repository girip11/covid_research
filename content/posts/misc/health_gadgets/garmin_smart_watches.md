# Garmin smartwatches for fitness tracking

## Vivosmart 5 features

- Wrist heart rate
- Body battery
- Pulse ox sensor
- Sleep monitoring
- Stress tracking
- Mindful breathing
- Hydration tracking
- Respiration tracking
- Fitness age
