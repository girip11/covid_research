# Heart rate monitoring ME and Long Covid

Heart rate monitor - aim is to stay under the anaerobic threshold.

> The point at which aerobic metabolism switches to anaerobic metabolism (when CO2 is higher than O2). The full name is the **ventilatory anaerobic threshold** (VAT).

- ME Patients have reduced VAT. Even a little exertion can cause the patients to exceed anaerobic threshold. And we patients experience what's called as post exertional malaise.

![PEM Timecourse](PEM_timescale.webp)

## Anaerobic threshold approximation based on Heart rate

**CPET** can give more accurate values for an individual.

- `(220 - your age) x 0.6` = anaerobic threshold in beats per minute

---

## References

- [Heart Rate Monitoring](https://www.physiosforme.com/heart-rate-monitoring)
- [Heart rate Variability](https://www.hrv4training.com/)
- [Heart Rate and Post-Exertional Crashes in ME/CFS](https://livewithcfs.blogspot.com/2011/02/heart-rate-and-post-exertional-crashes.html)
