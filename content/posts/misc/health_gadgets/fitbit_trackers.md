# Fitbit fitness trackers

## Fitbit charge 5

Features like ECG require fitbit premium subscription.

Features

- Energy levels via daily readiness score. This is similar to Body battery in Garmin. Paid feature.
- EDA and ECG scans are present but are premium features.
- Wrist based Heart rate
- Pulse ox sensor
- Sleep tracking

It has all the major features of Garmin Vivosmart 5. But some of the features require fitbit premium subscription.
