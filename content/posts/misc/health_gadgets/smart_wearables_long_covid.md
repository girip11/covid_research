# Smartwatch for long covid

## Primary Goal

- Heart rate monitoring
- Heart rate variability
- Stress
- Sleep tracking
- On demand ECG
- Steps/Activity tracking
- Calories
- SpO2

## Secondary goals

- Better batter life
- Design
- Affordability

## Benefits

Based on [heart rate monitoring survey][1], following benefits were reported.

- Understand PEM triggers better
- Helps with pacing

## Products

Smart wearbles/fitness trackers.

- [Fitbit trackers](https://www.fitbit.com/) - Fitbit charge 5 is interesting.
- [Garmin](https://www.garmin.com/en-US/p/605739) - Vivosmart 4 seems to be more popular among ME/CFS. Particularly it has **Body battery** feature which seems to be very helpful. Vivosmart 5 is the latest release. Costs 150$.
- [Withings scanwatch](https://www.withings.com/us/en/scanwatch) - Costs around 300$

Chest straps

- [Polar H10 chest straps](https://www.polar.com/en/sensors/h10-heart-rate-sensor) - I might prefer to wear it during walks or activity in addition to the smartwatch.

ECG monitors

- [AliveCor Kardia Mobile](https://alivecor.in/kardiamobile6l/) - This one is affordable and I see it's recommended by a veteran cardiologist Dr.Anthony Pearson in [his blog](https://theskepticalcardiologist.com/2021/03/07/a-comparison-of-alivecors-kardia-6l-and-kardia-single-lead-mobile-ecg-with-and-without-the-v2-algorithm/#comments)

**NOTE** - There are the ones I explored so far.

## Misc Notes

Preferred aerobic heart rate limits

- Men: `(220 - age) * 0.6`
- Women: `(220 - (age * 0.88)) * 0,6`

Heart rate variability - Interval between 2 heart beat cycles. It's lower when the sympathetic nervous system is active.

---

## References

- [Smart watch and long covid - Jo Kuehn twitter thread](https://twitter.com/jo_kuehn/status/1512804233527148553?s=20&t=j1aGRX1XfMkEKBBQbadrFg)
- [Heart rate monitoring](https://www.physiosforme.com/heart-rate-monitoring)
- [Heart rate monitoring - ME/CFS pacing survey result][1]

[1]: https://www.physiosforme.com/post/heart-rate-monitor-pacing-survey-results
