# Adrenal insufficiency in Long covid

## [Possible Adrenal Involvement in Long COVID Syndrome][1]

- Patient with long covid symptoms such as weakness, brain fog, dizziness and muscular and joint pain.
- Routine lab panels for inflammation, anemia, thyroid, LFT done. All normal.
- Adrenal stress Index(salivary cortisol) showed low levels of free cortisol
- Patient started hydrocortisone acetate supplementation.

### Key points

> Considering the psychosis during dexamethasone administration and HPA axes’ suppression that can occur even for 3 years after glucocorticoid suspension, to assess free cortisol, unbound from cortisol binding globulin, we performed an adrenal stress index, measuring salivary cortisol and DHEA-S
> One month after the treatment started, the patient was still on hydrocortisone supplementation and symptoms were reduced, but still not resolved.
> Adrenal cortex impairment was also implicated in hypocortisolism following COVID-19 infection.
> Salivary cortisol bypasses this problem, because only free cortisol can pass the salivary barrier.

## [Hormonal trends in patients suffering from long COVID symptoms][2]

- Depression and fatigue correlated with the serum levels of cortisol and free thyroxin(FT4) respectively.
- In addition, the ratios of plasma adrenocorticotropin to serum cortisol were decreased in patients with relatively high titers of serum SARS-CoV-2 antibody.

- Blood parameters measured were - ACTH, cortisol, growth hormone (GH), insulin-like growth factor (IGF)-I, free thyroxin (FT4), and thyrotropin (TSH).
- N =195, 9 excluded. Out of 186, 54 hospital admission in acute phase, 31 received oxygen/steroid therapy.
- The level of serum TSH was significantly higher and the ratio of FT4/TSH was lower in the moderate/high severity group than in the mild group, suggesting the possibility of “occult hypothyroid” status in severe conditions of initial infection.
- It was revealed that a relatively high titer of serum antibody (higher than 500 U/mL) decreases the ratios of ACTH/cortisol and FT4/TSH, indicating the possibilities of impaired function of ACTH in response to cortisol feedback and a certain level of latent hypothyroid status.

## [Hypothalamic-Pituitary Axis Function and Adrenal Insufficiency in COVID-19 Patients][3]

- In 2003, after the SARS outbreak, cases of pituitary insufficiency were identified.

- Physiological stress activates HPA axis, decreased cortisol metabolism and increase in serum cortisol levels.

- High titers of anti-ACTH antibodies were demonstrated in long COVID-19 patients indicating a different pathophysiological mechanism in HPA axis dysfunction

- Pituitary gland is highly sensitive to hypoxia, ischemia and hypovolemia.

- Mimicry of host ACTH by viral antigens may lead to binding of viral antibodies to host ACTH, causing secondary hypocortisolemia

- Basal ACTH, Basal cortisol measurements, low-dose ACTH stimulation test(LDST), High dose (250micrograms) ACTH stimulation test, Insulin tolerance test.

---

## References

- [Possible Adrenal Involvement in Long COVID Syndrome][1]
- [Hormonal trends in patients suffering from long COVID symptoms][2]
- [Hypothalamic-Pituitary Axis Function and Adrenal Insufficiency in COVID-19 Patients][3]

[1]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8537520/
[2]: https://www.jstage.jst.go.jp/article/endocrj/69/10/69_EJ22-0093/_html/-char/ja
[3]: https://karger.com/nim/article/30/1/215/862922/Hypothalamic-Pituitary-Axis-Function-and-Adrenal
