# Experience with Vortioxetine

- Treatment period March 2023 - April 2023.

- I was prescribed 5mg vortioxetine initially for couple of weeks and then it was titrated up to 10mg.
- I tolerated the medicine better compared to escitalopram. I was on it for a period of almost a month. When I titrated to 10mg I had some side effects or I felt weird. So I went back to 5mg. But I didnt see any improvement on long covid symptoms from it during this period.

- May be I should be on it for months to see some benefit I wouldnt know. I actually had lung issue that was causing all psychiatric symptoms. So treating the lung symptoms helped with the symptoms rather than the psychiatric medicine.
