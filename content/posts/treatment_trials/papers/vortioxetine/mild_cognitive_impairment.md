# Vortioxetine improves cognition in mild cognitive impairment

- 111 adults (single arm study phase 2) with mild cognitive impairments without depressive symptoms to take 5-10mg/day vortioxetine for 6 months.

Assessments

- Cognitive function [Montreal Cognitive Assessment (MoCA);
- Digit Symbol Substitution Test (DSST)],
- Disease severity [Clinical Dementia Rating (CDR)],
- Clinician-assessed improvement and safety.

> Vortioxetine treatment was associated with significant improvement in cognitive function and a favorable safety profile in community-dwelling older adults with MCI.

- After 6 months, 89.6% of subjects improved on disease severity.

## Vortioxetine improves physical and cognitive symptoms in patients with post-COVID-19 major depressive episodes

- Immune inflammatory changes - cause for depressive episodes.
- Vortioxetine - antiinflammatory and antioxidant properties.
- This treatment of 10 mg for 3 months improved physical features, cognitive functioning and reduced depressive symptoms.

---

## References

- [Vortioxetine improves cognition in mild cognitive impairment](https://pubmed.ncbi.nlm.nih.gov/34282748/)
- [Vortioxetine improves physical and cognitive symptoms in patients with post-COVID-19 major depressive episodes](https://www.sciencedirect.com/science/article/pii/S0924977X23000238)
