# Clinical effectiveness of vortioxetine in postcovid syndrome

> Vortioxetine improves cognitive function, is not associated with emotional dullness, normalizes circadian rhythms, and has an immunomodulatory effect.

- 58 patients with post covid syndrome, 22 on vortioxetine and remaining 36 on other antidepressants like escitalopram, sertraline, paroxetine and fevarin.
- Severity of depressive symptoms assessed by HADS questionnaire, cognitive sphere using DSST.

- Study duration - 3 months.

- Main symptoms of the study participants

  > fatigue, sleep disturbances, memory, and active attention, dyspnea, and emotional exhaustion. Other manifestations were decreased tolerance to exercises and muscle weakness, poor coordination, decreased muscle mass, pain, impaired static function and imbalance, hyposmia, and dysgeusia.

- Among the patients in the main group, 18 (81.8%) noted a **significant improvement in well-being after 3 months of treatment**.

## Limitations

- Its a small study. 18 people in the study group out of 22 felt significantly improved. But we don;t know if the status of the reported long covid symptoms.

---

## References

- [Clinical effectiveness of vortioxetine in postcovid syndrome](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9789434)
