# Pycnogenol

- Pycnogenol anticoagulant derived from maritime pine bark
- Reduces the platelet stickiness without affecting the bleeding time.

> Pycnogenol has been shown to improve arterial health by preventing arterial constriction, improving blood flow, leading to adequate tissue perfusion, preventing blood clots, and by exerting anti-inflammatory effects directly in blood vessels. - [Carolina Burki, MSc., is Director of Product Development at Horphag Research][1]

## Post Covid study

30 long haulers with pycnogenol 150mg thrice and 30 long haulers with standard care. Following were observed in the pycnogenol group

- Endothelial function and microcirculation improved
- Average rate of ankle swelling decreased significantly
- Kidney perfusion increased
- Inflammatory markers like hs-CRP and IL-6 significantly lower
- ESR lowered
- Mood and quality of life improved.
- Platelet and clotting factors were unaffected

## My takeaways

This supplement seems very promising. Larger trials might be required on safety and efficacy when used for long term.

---

## References

- [Pycnogenol-Nattokinase Combo Prevents In-Flight Venous Thrombosis](https://holisticprimarycare.net/topics/vitamins-a-supplements/pycnogenol-nattokinase-combo-prevents-in-flight-venous-thrombosis/)
- [Preventing Post-COVID symptoms with Pycnogenol: A new Clinical Study](https://pubmed.ncbi.nlm.nih.gov/34060731/)

1: "https://wholefoodsmagazine.com/columns/vitamin-connectionpreventing-post-covid-symptoms-with-pycnogenol-a-new-clinical-study/"
