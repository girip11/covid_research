# Hyperbaric oxygen therapy improves neurocognitive functions and symptoms of post-COVID condition: randomized controlled trial

## Terminologies

- Ageusia - Loss of taste

## Study

- Randomized, sham controlled and double blind trial
- 73 participants with confirmed covid infection with persisting symptoms after 3 months of acute phase.
- 40 Sessions of HBOT. HBOT group `n=37` and sham `n=36`.
- 5 sessions a week for 8 weeks.
- HBOT protocol breathing 100% oxygen at 2 ATA for 90 minutes while the sham protocl involved breathing 21% oxygen at 1.03 ATA for 90 minutes.
- Followup assessments at baseline and 1-3 weeks after the last session.

## Observations

- Significant improvements observed in the brain MRI in various regions of the brain possibly due to increased perfusion.
- HBOT can induce neuroplasticity and improve cognitive, psychiatric, fatigue, sleep and pain symptoms of patients suffering from post-COVID-19 condition.

## Notes

> Preclinical and clinical studies have demonstrated several neuroplasticity effects including anti-inflammatory, mitochondrial function restoration, increased perfusion via angiogenesis and induction of proliferation and migration of stem cells.

Brain perfusion imaging revealed the following

- Significant gray matter increase in the HBOT group.
- Significant group by time interactions were observed in various regions of the brain.

Some of the side effects captured from the study are barotrauma, ear pain, presyncope, chest pain/epigastric pain, fever, palpitation, headache, hypertension.

- Hypometabolism of the frontal lobe is implicated with fatigue.

- Brain MRI scan performed with 3T scanner

> The MRI protocol included T2-weighted, 3D fluid attenuated inversion recovery (FLAIR), susceptibility weighted imaging (SWI), pre- and post-contrast high-resolution MPRAGE 3D T1-weighted, dynamic susceptibility contrast (DSC) for calculating whole-brain quantitative perfusion maps, and diffusion tensor imaging (DTI) for microstructure changes in grey and white matter determination.

This study did measure many secondary outcomes which will be published in the future.

> Additional secondary outcomes including neuro-physical evaluation, cardiopulmonary exercise test, echocardiography, and functional brain imaging will be presented in future manuscripts.

---

## References

- [Hyperbaric oxygen therapy improves neurocognitive functions and symptoms of post-COVID condition: randomized controlled trial](https://www.nature.com/articles/s41598-022-15565-0)
