# HBOT for long covid

## [1. Hyperbaric oxygen therapy for the treatment of long COVID: early evaluation of a highly promising intervention][1]

- 10 Patients were given 10 sessions of HBOT in 12 days with each session lasting 105 minutes (30-30-30 with 5-minute breaks). Atmospheric pressure at 2.4 atmospheres.

- Significant improvement in fatigue and cognitive issues.

## [2-Hyperbaric oxygen treatment: Clinical trial reverses two biological processes associated with aging in human cells][2]

- HBOT can reverse the aging of blood cells and hence the aging process itself.
- The researchers exposed 35 healthy individuals aged 64 or over to a series of 60 hyperbaric sessions over a period of 90 days.

> In this study, Only three months of HBOT were able to elongate telomeres at rates far beyond any currently available interventions or lifestyle modifications. With this pioneering study, we have opened a door for further research on the cellular impact of HBOT and its potential for reversing the aging process."

## My takeaways

- Issue with the [study #1][1] is after the 10 sessions did the baseline of these fatigue and cognitive issues shifted permanently or did the symptoms came back to their original state after those sessions. Long term followup is missing and its crucial.

---

## References

[1]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8806311/
[2]: https://www.sciencedaily.com/releases/2020/11/201120150728.htm
