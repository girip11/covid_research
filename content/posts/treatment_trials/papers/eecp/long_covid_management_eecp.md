# Enhanced external counterpulsation for management of symptoms associated with long COVID

- `N=16`

## Main outcome measures

- Patient Reported Outcome Measurement Information System (PROMIS) Fatigue
- Seattle Angina Questionnaire (SAQ)
- Duke Activity Status Index (DASI)
- 6-Minute Walk Test (6MWT)
- Canadian Cardiovascular Society (CCS) Angina Grade
- Rose Dyspnea Scale (RDS)
- Patient Health Questionnaire (PHQ-9)

---

## References

- [Enhanced external counterpulsation for management of symptoms associated with long COVID](https://www.sciencedirect.com/science/article/pii/S2666602222000222)
