# CISP - COVID-19 INDUCED SECONDARY PELLAGRA

I had contacted Dr. Ade Wentzel regarding the NAD+ stack for long covid. He was very graceful enough to answer all my questions, and he shared the following resources with me.

## Dr. Ade's personal encounter with long covid

After getting mild Covid-19 on 6 Dec 2019 on a flight, with coughing travelers, from Cairo to Milan, and initially recovering, I became really unwell 3 months later, after doing 6 hrs of strenuous exercise.

I experienced severe effort intolerance, with chest pain, high heart rate, dizziness, bland mood, diarrhea and vision problems. I experienced 31 of the 50 Survivor Corps long-haul symptoms.

**I AM NOW WELL AFTER A LONG JOURNEY**. Here is the story.

Luckily, by chance, while designing oxygen masks and a covid hospital in my hometown during lock-down preparation, I met up with Robert Miller, a Cape Town bio-hacker who had made the observation that those most at risk were also the same groups that had pre-existing low NAD+.

Robert Miller, Prof Guy Richard's and I went on to research this observation and published in Medical Hypothesis-COVID-19: NAD+ deficiency may predisposes the aged, obese and type2 diabetics to mortality through its effect on SIRT 1 activity.

Subsequently, papers confirming COVID-19 causes low NAD+ through its effect on NAD+ metabolism, have confirmed the above hypothesis.

While writing the paper, I had researched pellagra, a niacin (nicotinic acid) vitamin deficiency, rarely seen today. A deficiency that causes low NAD+ resulting in numerous symptoms in metabolically active tissues. Incidentally, the last major pellagra outbreaks occurred in the 1930's in Italy and the SouthEast USA.

When comparing the symptoms of pellagra to long-haul COVID-19, the symptoms are virtually interchangeable.

- Dermatitis
- Diarrhea
- Fatigue and apathy
- Temperature
- Loss of smell
- Loss of taste
- Loss of hair
- Red skin lesions
- Smooth beefy red tongue
- Aggression
- Insomnia
- Weakness
- Mental confusion
- Ataxia
- Paralysis of extremities
- Peripheral neuritis
- Edema
- Nutritional deficiency cardiomyopathy.
- Multi organ failure
- Dementia
- Death

Above are ALL pellagra symptoms. We know that COVID-19 causes low NAD+ and that this low NAD+ results in COVID-19 Induced Secondary Pellagra (CISP), and that pellagra and long-haul symptoms are pretty much interchangeable.

Long-haul NAD+ related symptoms also respond to treatment with Nicotinic acid (niacin). I have attached the article and a whiteboard presentation explaining the lack of NAD+ and Tryptophan and how to treat it.

Supplementation with Nicotinic Acid is easily accessible, safe and cheap and probably should be used before, during and after COVID-19 infection.

## Publications

- [Post-acute COVID-19 sequela - 'COVID long hauler'](https://journals.co.za/doi/10.18772/26180197.2021.v3n2a4)

- [COVID-19: NAD+ deficiency may predispose the aged, obese and type2 diabetics to mortality through its effect on SIRT1 activity](https://www.sciencedirect.com/science/article/pii/S0306987720314742)

## Video resources

- [Pellagra - A Medical Mystery - Extra History](https://youtu.be/reYKBgdrZsM)
- [The COVID-19 Code](https://youtu.be/dRNNcd2Lj6w)
- [NAD+ metabolism and Long covid video](https://vimeo.com/637590579)

The below videos are available on the [Gez Medinger's youtube channel](https://www.youtube.com/c/RUNDMC1)

- [First Effective Treatment for Long Covid | Stunning Data from Huge New Study](https://youtu.be/9-3V3h0ncIA)
- [The Biology of Long Covid | Part 2: The Answers - with Dr Ade Wentzel](https://youtu.be/C3w7skYHcSg)
- [The Puzzle Comes Together - Why Dysfunctional Metabolism Might Explain Long Covid](https://youtu.be/ZFPleh6z7io)

## Other resources

- [The Team of Doctors and Biohackers Who Seem to Be Successfully Treating Long Covid](https://nkalex.medium.com/the-team-of-front-line-doctors-and-biohackers-who-seem-to-have-solved-long-covid-5f9852f1101d)

- [Health & Wellness: Cracking the Covid Code](https://omny.fm/shows/afternoons-with-pippa-hudson/health-wellness-cracking-the-covid-code#sharing)

- [Inside Covid-19: Discovery Vitality travel perks; Vitamin B, Zinc could hold answer to keeping worst symptoms at bay](https://www.biznews.com/health/2020/11/08/covid-19-17)

- [Cracking the Covid-19 code: Does bio-hacking have a role to play?](https://www.news24.com/news24/southafrica/news/cracking-the-covid-19-code-does-bio-hacking-have-a-role-to-play-20200929)

## Treatment protocol

![NAD+ deficiency treatment protocol](nadplus_treatment_stack.jpeg)

- In South Africa, all ingredients of the treatment protocol are available in 1 tablet **CIPLA SIRTMUNE**.

- Additionally, NAC, Omega-3 and Coq10 can be added to the protocol if required.

---

## Publications on NAD+ metabolism

- [NAD+ Metabolism, Metabolic Stress, and Infection](https://www.frontiersin.org/articles/10.3389/fmolb.2021.686412)
- [NAD+ metabolism: pathophysiologic mechanisms and therapeutic potential](https://www.nature.com/articles/s41392-020-00311-7)

## Covid and NAD+ metabolism

- [NAD+ in COVID-19 and viral infections](https://www.sciencedirect.com/science/article/pii/S1471490622000254)
- [IMPLICATIONS OF THE NADASE CD38 IN COVID PATHOPHYSIOLOG](https://journals.physiology.org/doi/pdf/10.1152/physrev.00007.2021)
- [The COVID-19 "Bad Tryp" Syndrome: NAD/NADH+, Tryptophan Phenylalanine Metabolism and Thermogenesis like Hecatomb - The Hypothesis of Pathophysiology Based on a Compared COVID-19 and Yellow Fever Inflammatory Skeleton](https://www.clinmedjournals.org/articles/jide/journal-of-infectious-diseases-and-epidemiology-jide-8-243.php?jid=jide)
