# Pentoxifylline for acute covid

Clinical trials

- [Efficacy of Pentoxifylline as Add on Therapy in COVID19 Patients](https://clinicaltrials.gov/ct2/show/NCT04433988)
- [RECLAIM: Recovering From COVID-19 Lingering Symptoms Adaptive Integrative Medicine (RECLAIM)](https://ichgcp.net/clinical-trials-registry/NCT05513560)

Below studies were done on acute covid patients

## Pentoxifylline as a Potential Adjuvant Therapy for COVID-19: Impeding the Burden of the Cytokine Storm

- Previous studies, PTX efficacy in the treatment of various pulmonary diseases.
- PTX is approved for the management of intermitten claudication in patients suffering from chronix occlusive arterial disease of the limbs.

> Currently, PTX is mainly used in the treatment of intermittent claudication resulting from peripheral artery disease, in which it improves blood viscosity, stimulates fibrinolysis, makes erythrocytes more elastic and less prone to adhesion, and downregulates platelet aggregation.

- PTX has been documented to have anti-inflammatory and immunomodulatory effects plus antithrombotic and antiviral effects.

- Fibrosis in Covid 19 hypothesis -> Alveolar damage -> overexpression of inflammatory cytokines -> activation of fibroblasts -> deposition of collagen in the pulmonary parenchyma

> Given all of the above data, we hypothesize that PTX may be a potent adjuvant drug for preventing and treating COVID-19-related pulmonary fibrosis. We call for large, population-based clinical trials repurposing this well-known drug with a previously established safety profile.

- Elevated D-Dimer, fibrinogen serumlevels, prolonger prothrombin times and low platelet counts are some of the findings in Covid19. These alterations are similar to disseminated intravascular coagulation(DIC) seen in sepsis.

- PTX was evaluated for the treatment of DIC in sepsis. PTX was effective in reducing d-dimer, fibrinogen level, platelet count increased and prothrombin time shortened.

- As PTX can improve RBC elasticity(one study showed RBC elasticity was reduced in long covid patients), it can increase blood flow and hence tissue oxygenation. Helpful in treating hypercoagulation and subsequent hypoxia

## Pentoxifylline effects on hospitalized patients with COVID19: A randomized, double-blind clinical trial

- Double blind, placebo controlled randomized clinical trial
- Intervention group - hospitalized patients PTX capsules at 400mg thrice a day for 10 days plus infterferon, lopinavir/ritonavir and hydroxychloroquine.
  > The results of our study did not show any superiority of PTX over placebo in improving the clinical outcomes of patients with COVID-19.
  > Although PTX led to a significant decrease in IL-6 levels as an inflammatory biomarker as well as a significant increase in GSH levels as an antioxidant, no differences were found in LPO, LDH and CRP.

This study mentions that PTX has no effect on LDH levels, but the next study mentions PTX reduces LDH levels. But both agree upon no impact on hospitalization, severity.

## Pentoxifylline decreases serum LDH levels and increases lymphocyte count in COVID-19 patients: Results from an external pilot study

- 38 patients with moderate and severe covid. 26 patients were randomized.
- Intervention group - PTX plus standard therapy, control group - standard therapy.
- PTX group - 64.25% increase in lymphocyte count and 29.61 reduction in serum LDH.
- But Days of hospitalization, mortality and intubation - no statistically significant diff with the control group.

## Pentoxifylline: A Drug with Antiviral and Anti-Inflammatory Effects to Be Considered in the Treatment of Coronavirus Disease 2019

- PTX, a drug with anti-inflammatory, immunomodulatory, and bronchodilatory effects, has previously been shown to inhibit several viral infections.
- PTX is a phosphodiesterase inhibitor that increases the levels of **cyclic adenosine monophosphate**, which in **turn activates protein kinase**, leading to a reduction in the synthesis of pro-inflammatory cytokines and immune cell migration.
- Treatment proposed for acute covid in this paper.

## A raising dawn of pentoxifylline in management of inflammatory disorders in Covid-19

- PTX adenosine 2 receptor antagonist.

> PTX is a non-selective phosphodiesterase inhibitor that increases intracellular cyclic adenosine monophosphate which stimulates protein kinase A and inhibits leukotriene and tumor necrosis factor. PTX has antiviral, anti-inflammatory and immunomodulatory effects, thus it may attenuate SARS-CoV-2-induced hyperinflammation and related complications.
> PTX can reduce hyper-viscosity and coagulopathy in Covid-19 through increasing red blood cell deformability and inhibition of platelet aggregations

> PTX **decreases blood viscosity by reducing fibrinogen plasma levels, increasing red blood cell flexibility, and inhibiting platelet aggregation**

---

## References

- [Pentoxifylline as a Potential Adjuvant Therapy for COVID-19: Impeding the Burden of the Cytokine Storm][1]
- [Pentoxifylline effects on hospitalized patients with COVID19: A randomized, double-blind clinical trial][2] -[Pentoxifylline decreases serum LDH levels and increases lymphocyte count in COVID-19 patients: Results from an external pilot study][3]
- [Hypothesis: Pentoxifylline is a potential cytokine modulator therapeutic in COVID-19 patients][4]
- [A raising dawn of pentoxifylline in management of inflammatory disorders in Covid-19][5]
- [Pentoxifylline: A Drug with Antiviral and Anti-Inflammatory Effects to Be Considered in the Treatment of Coronavirus Disease 2019][6]

[1]: "https://www.ncbi.nlm.nih.gov/pmc/articles/pmc8617922/"
[2]: "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8492603/"
[3]: https://www.sciencedirect.com/science/article/pii/S1567576920336766
[4]: https://bpspubs.onlinelibrary.wiley.com/doi/10.1002/prp2.631
[5]: https://link.springer.com/article/10.1007/s10787-022-00993-1
[6]: https://www.karger.com/Article/Abstract/512234
