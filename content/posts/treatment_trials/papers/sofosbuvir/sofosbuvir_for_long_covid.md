# Sofosbuvir for long covid

---

## References

- [SARS-CoV-2 infects human brain organoids causing cell death and loss of synapses that can be rescued by treatment with Sofosbuvir](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.3001845)
- [Organoid Study Shows Already Approved Drug Could Treat Long-COVID](https://www.laboratoryequipment.com/591821-Organoid-Study-Shows-Already-Approved-Drug-Could-Treat-Long-COVID/)
- [The potential of Sofosbuvir against COVID-19 neurological symptoms
  ](https://www.news-medical.net/news/20221125/The-potential-of-Sofosbuvir-against-COVID-19-neurological-symptoms.aspx)
- [UCSD Researchers Find New Drug Sofosbuvir is Helpful to Fight COVID-19 Neurological Symptoms](https://ucsdguardian.org/2022/11/20/ucsd-researchers-find-new-drug-sofosbuvir-is-helpful-to-fight-covid-19-neurological-symptoms/)
