# Sofosbuvir for Acute covid treatment

## [Effect of sofosbuvir-based treatment on clinical outcomes of patients with COVID-19: a systematic review and meta-analysis of randomised controlled trials][1]

- 687 patients with covid were included, 377 treated with sofosbuvir.
- Mortality lower in the study group.
- Clinical recovery rate was higher in the study group
- Lower need for ventilator and ICU admissions in the study group. Shorter hospital stay.

## [Sofosbuvir and daclatasvir for the treatment of COVID-19 outpatients: a double-blind, randomized controlled trial][2]

- Outpatients with mild covid were part of the study. Conducted in 2020
- Study group - sofosbuvir/daclatasvir and hydroxychloroquinone `n=27`. Control group only hydroxychloroquinone `n=28`.
- Hospital admissions no statistical significance. But after 1 month follow up, less patients reported fatigue in the study group compared to control group `n=2 vs n=16`

## [Efficacy and safety of the sofosbuvir/velpatasvir combination for the treatment of patients with early mild to moderate COVID-19][3]

- study group `n=120` with mild/moderate covid given SOF/VEL, control group `n=90` received standard care.
- SOF/VEL tablets(400/100mg) once daily for 9 days

> In non-HCV positive COVID-19 patients, treatment with SOV/VEL was discontinued after 9 days of treatment, while treatment of patients co-infected with HCV continued for 12 weeks.

- 83% of patients in the study group tested negative after 5-14 days of starting treatment while it was 13% in the control group.
- No disease progression in the treatment group while 24% required intensive treatment in the control group.

> **Early SOF/VEL treatment** in mild/moderate COVID-19 seems to be safe and effective for faster elimination of SARS-CoV-2 and to prevent disease progression.

## [Efficacy and safety of sofosbuvir/velpatasvir versus the standard of care in adults hospitalized with COVID-19: a single-centre, randomized controlled trial][4]

- Study conducted in moderate/severe covid patients.
- SOF/VEL arm `n=40` and control arm with standard care, `n=40`
- No difference in primary and secondary outcomes between the two groups.

## [Antivirals for adult patients hospitalised with SARS-CoV-2 infection: a randomised, phase II/III, multicentre, placebo-controlled, adaptive study, with multiple arms and stages. COALITION COVID-19 BRAZIL IX – REVOLUTIOn trial][5]

> REVOLUTIOn is a randomised, parallel, blinded, multistage, superiority and placebo controlled randomised trial conducted in 35 centres in Brazil
> 255 participants were enrolled and randomly assigned to atazanavir (n = 64), daclatasvir (n = 66), sofosbuvir/daclatasvir (n = 67) or placebo (n = 58).
> **Compared to placebo group, the change from baseline to day 10 in log viral load was not significantly different for any of the treatment groups**

## [Efficacy of Sofosbuvir plus Ledipasvir in Egyptian patients with COVID-19 compared to standard treatment: Randomized controlled trial][6]

> The participants were randomized equally into the intervention group received Sofosbuvir/ledipasvir (S.L. group), and the control group received Oseltamivir, Hydroxychloroquine, and Azithromycin (OCH group).
> Two hundred and fifty patients were divided equally into each group. Both groups were similar regarding gender, but age was higher in the S.L. group (p=0.001). In the S.L. group, 89 (71.2%) patients were cured, while only 51 (40.8%) patients were cured in the OCH group. The cure rate was significantly higher in the S.L. group (RR=1.75, p<0.001). Kaplan-Meir plot showed a considerably higher cure over time in the S.L. group (Log-rank test, p=0.032). There were no deaths in the S.L. group, but there were six deaths (4.8%) in the OCH group (RR=0.08, p=0.013). Seven patients (5.6%) in the S.L. group and six patients (4.8%) in the OCH group were admitted to ICU (RR=1.17, P=0.776). There was no significant difference between treatment groups regarding Total Leukocyte Count, Neutrophils count, Lymph and Urea.
> **Sofosbuvir/ledipasvir is suggestive of being effective in treating patients with moderate COVID-19 infection.** Further studies are needed to compare Sofosbuvir/ledipasvir with the new treatment protocols.

## References

- [Effect of sofosbuvir-based treatment on clinical outcomes of patients with COVID-19: a systematic review and meta-analysis of randomised controlled trials][1]
- [Sofosbuvir and daclatasvir for the treatment of COVID-19 outpatients: a double-blind, randomized controlled trial][2]
- [Efficacy and safety of the sofosbuvir/velpatasvir combination for the treatment of patients with early mild to moderate COVID-19][3]
- [Efficacy and safety of sofosbuvir/velpatasvir versus the standard of care in adults hospitalized with COVID-19: a single-centre, randomized controlled trial][4]
- [Antivirals for adult patients hospitalised with SARS-CoV-2 infection: a randomised, phase II/III, multicentre, placebo-controlled, adaptive study, with multiple arms and stages. COALITION COVID-19 BRAZIL IX – REVOLUTIOn trial][5]
- [Efficacy of Sofosbuvir plus Ledipasvir in Egyptian patients with COVID-19 compared to standard treatment: Randomized controlled trial][6]

[1]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8817946/
[2]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7798988/
[3]: https://www.nature.com/articles/s41598-022-09741-5
[4]: https://academic.oup.com/jac/article/76/8/2158/6284235?login=false
[5]: https://www.thelancet.com/journals/lanam/article/PIIS2667-193X(23)00040-6/fulltext
[6]: https://www.medrxiv.org/content/10.1101/2021.05.19.21257429v1.full
