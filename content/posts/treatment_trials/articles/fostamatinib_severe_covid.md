# Afucosylation of anti-SARS-CoV-2 IgG

Multiple studies(see references section) seem to echo the following.

**High titers of low fucosylated sars-cov2 spike antibody complex expressed by infected cells** early during the infection activate the macrophages which in turn release proinflammatory cytokines **disrupting the pulmonary endothelial barrier** subsequently causing microvascular thrombosis through platelet activation.

[This paper][3] proposes using **fostamatinib** to counteract the macrophage activation by fucose deficient antispike-IgG.

## My takeaways

- Fostamatinib needs RCT to find its efficacy. Its very promising. This could stop progression of disease when administered at early stage in the illness. I am seeing phase 2 trials completed and phase 3 trials are in progress.

---

## References

- [Early non-neutralizing, afucosylated antibody responses are associated with COVID-19 severity][1]
- [Proinflammatory IgG Fc structures in patients with severe COVID-19][2]
- [High titers and low fucosylation of early human anti-SARS-CoV-2 IgG promote inflammation by alveolar macrophages][3]
- [Complement activation induces excessive T cell cytotoxicity in severe COVID-19][4]

[1]: https://www.science.org/doi/10.1126/scitranslmed.abm7853
[2]: https://www.medrxiv.org/content/10.1101/2020.05.15.20103341v2
[3]: https://www.science.org/doi/10.1126/scitranslmed.abf8654
[4]: https://pubmed.ncbi.nlm.nih.gov/35032429/
