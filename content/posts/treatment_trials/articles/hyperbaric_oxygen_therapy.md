# Hyperbaric Oxygen Therapy for Long Covid

## Patient Testimony from News

### Leanne Lawrence

Patient had the following symptoms post catching covid in January 2021.

- Severe fatigue
- Stomach pain and issues with gastric system
- Headaches
- Tremors in hands
- Pins and needles all over the body
- Hair loss
- Menstrual cycle affected

Improvement was observed just after two weeks of starting the treatment. Following observations were made post the therapy.

- No heart palpitations
- Severe fatigue was resolved
- Pins and needles stopped

### Steve Stickley

Caught covid in October 2020. Symptoms seemed to relapse from November 2020 - February 2021.

He had the Pfizer vaccine on February 2021 and his health went downhill from there for the next few months.

Symptoms

- Severe fatigue
- Loss of strength in legs
- Lower body spasms
- Reduced oxygen saturation and breathing issues

He had `15+` sessions. Symptom resolution

- Oxygen saturation normal (improved from `88-91%` to `95+`)
- Improved energy levels

But even post the therapy he continues to feel fatigue, weak legs and brain fog.

## Social media

### Christin Polak Blyumin

I didn't know about her symptoms of long covid. But her experience was shared by another long hauler to me.

![Christin Polak](../assets/christin_polak_hbot.jpeg)

Important thing to note is she was also on Patterson's protocol. So I am not sure how much HBOT contributed to the improvement.

### Gez Medinger

> - 8th session of HBOT at <http://thewellnesslab.com> in two weeks.Genuinely feeling an improvement in energy and PEM resistance - am on anticoagulants too but the combo for sure is making a difference. And it makes sense when you think about it! - [HBOT improving energy levels](https://twitter.com/gezmedinger/status/1470380234042187778?s=20&t=3NrdRRAhLTj6x0jNRhaGuQ)
>
> - Generally it takes 5-10 sessions of HBOT to make a lasting difference. Although it helped to be on anti-coag meds at the same time. - [HBOT sessions](https://twitter.com/gezmedinger/status/1482337529642852352?s=20&t=3NrdRRAhLTj6x0jNRhaGuQ)
>
> - I’ve done a few sessions of HBOT at [thewellnesslab](http://thewellnesslab.com). Found it particularly helped with **fatigue, brain fog, headaches and raised the overall baseline**. Don’t have many specific neuro symptoms so can’t comment on those.- [Symptoms resolution](https://twitter.com/gezmedinger/status/1476905299898511361?s=20&t=3NrdRRAhLTj6x0jNRhaGuQ)
>
> - Yes was on anti-coags and doing much better with HBOT but the booster sent me right back. I went out to the alps whilst still in that post-booster malaise and feel like it’s been mostly shook off now. Let’s see if it sticks when I’m home! - [HBOT and anticoagulants](https://twitter.com/gezmedinger/status/1482351644214022152?s=20&t=3NrdRRAhLTj6x0jNRhaGuQ)

Important thing to note is how much HELP apheresis and triple blood thinning therapy would have contributed to the symptom resolution along with HBOT.

## My takeaways

- HBOT seems to be helping with improving the energy levels leading to reduced fatigue.
- I would be interested in knowing about the outcomes on brain fog particularly on the memory side.
- It would be great to get feedback on HBOT and dysautonomia symptoms.

Is it possible to put out a survey to capture people experiences with HBOT?

---

## References

- [Long Covid: 'Hyperbaric chamber treatment transformed my life'](https://www.bbc.com/news/uk-scotland-north-east-orkney-shetland-58724980)
- [How 'remarkable' therapy helped man with Long Covid feel 'healthy again'](https://www.nottinghampost.com/news/local-news/how-remarkable-therapy-helped-man-5710217)
