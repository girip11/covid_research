# Enhanced External Counter pulsation

## [Enhanced External Counterpulsation Offers Potential Treatment Option for Long COVID Patients][1]

- Long covid patients(small set `n=50`) with and without coronary artery disease when treated with EECP(15-35 hours) showed improvements with symptoms like fatigue, breathing difficulties, chest discomfort, brain fog etc.

- EECP improves the heart and brain perfusion as well as improves the endothelial function.

- All patients in the study were assessed using Seattle Angina questionnaire7, Duke activity status index, PROMIS fatigue instrument, Rose Dyspnea Scale and 6-minute walk test.

Outcome

- Health Status using the SAQ7 tool improved by 25 points (Range 0-100)
- Functional capacity using the DASI assessment improved by 20 points (range 0-58.2)
- Fatigue levels using the PROMIS score decreased by 6 points (Range 4-20)
- Shortness of Breath using the RDS decreased in 50% of patients
- Walking capacity (6MWT) in 6 minutes increased by 178 feet

## [Treating COVID-19 Long Haulers with EECP][3]

- A 38 year old patient with long covid experiencing fatigue, shortness of breath and brain fog took EECP treatment 1 hour sessions, 3 times per week. After 5 weeks of EECP treatment patient returned to precovid health and fitness.

---

## References

- [Enhanced External Counterpulsation Offers Potential Treatment Option for Long COVID Patients][1]
- [Long-COVID Symptoms and Novel Enhanced External Counterpulsation Therapy][2]
- [Treating COVID-19 Long Haulers with EECP][3]

[1]: https://www.acc.org/About-ACC/Press-Releases/2022/02/14/14/25/Enhanced-External-Counterpulsation-Offers-Potential-Treatment-Option-for-Long-COVID-Patients
[2]: https://www.thecardiologyadvisor.com/general-medicine/long-covid-symptoms-and-enhanced-external-counterpulsation/
[3]: https://flowtherapy.com/resource/treating-long-covid-with-eecp/
