# Coagulation decision tree from economical aspect

Coagulation profile has so many tests and it can be very expensive to take all the tests at one go, unless you have hefty medical insurance that covers it all.

- Start with homocysteine, d-dimer and microclot testing(yet to be commercially available).

- If none of the above tests yield anything, then there is nothing else to proceed further from the coagulation front. But you may still optionally want to test for [APS](../blood_tests.md#AntiPhospholipid%20Syndrome) to rule it out completely.

- If high homocysteine, then [proceed to test further](../blood_tests.md#Homocysteine%20metabolism).

- If we test for microclots, then we could test for TEG(ThromboElastoGraphy) to confirm the blood is indeed hypercoagulable and not hypocoagulable. This is important before we start the medication.

- If the d-dimer is high, then proceed with rest of the tests in the [coagulation profile](../blood_tests.md#Coagulation) which are Fibrinogen, PT, APTT, INR, vWF.

- Post testing positive for either microclots or elevated D-dimer, it is better to take the [APS antibodies test](../blood_tests.md#AntiPhospholipid%20Syndrome) to see if the anticoagulant/antiplatelets have to be continued or can be discontinued post the course.

Finally when you are deep down in to the coagulation issue, take the remaining tests like Protein C and S and antithrombin.
