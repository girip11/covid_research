# AntiPhospolipid Syndrome

## Assessment

- Discuss with your doctor if you had previous clotting episodes. Other risk factors like for thrombosis must be addressed.

- APS has a genetic component, although there is not a direct transmission from parent to offspring.

## Tests

- [Anti Cardiolipin antibodies IgG and/or IgM](https://www.testing.com/tests/cardiolipin-antibodies/)
- [Anti Beta 2 glycoprotein I IgG and/or IgM](https://www.testing.com/tests/beta-2-glycoprotein-1-antibodies/)
- [Lupus anticoagulant testing](https://www.testing.com/tests/lupus-anticoagulant-testing/)

## Criteria

- It is common to have only one of these antibodies, but some patients have two or even all three.
- To confirm a diagnosis of antiphospholipid syndrome, the antibodies must appear in your blood at least twice, in tests conducted 12 or more weeks apart.

> The updated international consensus (Sydney) classification (ICS) criteria for definite antiphospholipid syndrome1 require the presence of a lupus anticoagulant (LA) and/or IgG or IgM anticardiolipin antibodies (aCL) present in medium or high titre (i.e. >40 GPL or MPL or >99th percentile), and/or anti-β2glycoprotein-1 (aβ2GPI) (IgG and/or IgM) >99th percentile. These aPL should be persistent, defined as being present on two or more consecutive occasions at least 12 weeks apart. - [Diagnosis of antiphospholipid syndrome in routine clinical practice][1]

---

## References

- [What Dysautonomia Patients Should Know About Antiphospholipid Syndrome](https://dysautonomiainternational.org/blog/wordpress/what-dysautonomia-patients-should-know-about-antiphospholipid-syndrome/)

- [Diagnosis of antiphospholipid syndrome in routine clinical practice][1]

[1]: https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC4108293/
