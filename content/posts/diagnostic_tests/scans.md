# Scans for Long Covid

I have jotted down the following procedures with only **Long Covid** in mind. Post covid different people are left with different issues. In this write up, I try to list all these based on what I have read from Long Covid patient experiences on blog, articles and twitter.

This is **purely for information purpose only**. Always your doctor should decide what test needs to be taken for what they think could be causing it.

## Pulmonary

Usually ordered by doctors, but the below two doesn't seem to help much in Long covid.

- Chest Xray
- CT scan Thorax

Many patients have had lung abnormalities detected from below tests

- Lung Function Tests - Peak flow test, Spirometry, Gas transfer test, lung volume measurement, Diffusing Capacity of Carbon monoxide(DLCO)

- Dual-energy CT **Pulmonary Angiogram** to look for clots in the lungs

Following three tests have been the most promising so far in detecting abnormalities in the lung diffusion.

- Hyperpolarized Xenon MRI - to study lung gas transfer in patients with dyspnea/shortness of breath. This technique does not involve any radiation.
- SPECT scan of the lungs
- **VQ scan(Ventilation Perfusion scan)** - involves two scans: one measures how well air is able to flow through your lungs and the other measures where blood flows in your lungs. In the ventilation scan, patients inhale Xenon(133) which is radioactive and emits Gamma rays(ionizing radiation).

Oxygen extraction by muscles and exercise intolerance detection using below.

- [6 Minute walk test](https://www.medicalnewstoday.com/articles/6-minute-walk-test#purpose)
- A **CardioPulmonary Exercise Test** (CPET) or [iCPECT(invasive CPECT)](https://pubmed.ncbi.nlm.nih.gov/34389297/) lets your doctor see how your lungs, heart and muscles react together when you exercise. This could measure $Vo_{2}$ max.

## Cardiovascular

- ECG
- 24 hour Holter monitor
- Echocardiogram
- Cardiac MRI - This seemed to be the most useful one.

## Brain

- Brain MRI
- SPECT/PET scan
