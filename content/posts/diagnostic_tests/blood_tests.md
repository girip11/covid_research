# Long Covid Blood Work Manual

I have jotted down the following tests with only **Long Covid** in mind. Post covid different people are left with different issues. In this write up, I try to list all these tests based on what I have read from Long Covid patient experiences on blog, articles and twitter.

This is **purely for information purpose only**. Always your doctor should decide what test needs to be taken for what they think could be causing it.

Also don't expect all the listed tests to be available in all countries. I have tried to keep this list as global as possible, so people all over the world can benefit.

**NOTE**- If you are a Long Covid patient, use this list only as a source of information. You might not have every condition that will require you to test for all of these below. Though we long covid folks know that, over the course of time we have actually taken many of these tests listed below.

## General tests

- [Complete Blood Count](https://www.testing.com/tests/complete-blood-count-cbc/)
- [Lipid Profile](https://www.testing.com/tests/lipid-panel/)
- [Liver Function Tests](https://www.testing.com/tests/liver-panel/)
- [Kidney function tests including EGFR](https://www.testing.com/tests/renal-panel/)
- [HBA1c](https://www.testing.com/tests/hemoglobin-a1c/)
- [Thyroid function with TPO antibodies](https://www.testing.com/thyroid-function-testing/)

## Bone Weakness

- [Vitamin D](https://www.testing.com/tests/vitamin-d-tests/)
- [Calcium](https://www.testing.com/tests/calcium/)

## Iron Deficiency Anemia

- [Serum Ferritin](https://www.testing.com/tests/ferritin/) and [Iron deficiency](https://www.testing.com/tests/iron/)

## Tissue damage

- [Lactate dehydrogenase](https://www.testing.com/tests/lactate-dehydrogenase-ldh/)

## Pulmonary

- [Arterial Blood Gas analysis](https://www.testing.com/tests/blood-gases/)

## Heart

- [Troponin](https://www.testing.com/tests/troponin/)
- [Brain Natriuretic Peptide BNP](https://www.testing.com/tests/bnp-and-nt-probnp/)

## Viral Reactivations

- [EBV](https://www.testing.com/tests/epstein-barr-virus-ebv-antibody-tests/)
- Human Herpes Virus 6 antibodies (IgM and IgG)
- [Shingles](https://www.testing.com/tests/chickenpox-and-shingles-tests/)
- Cytomegalovirus antibodies (IgM, IgG)

## Inflammatory markers

- [Erythrocyte Sedimentation Rate](https://www.testing.com/tests/erythrocyte-sedimentation-rate-esr/)
- [Creatine Phosphokinase(CPK) aka Creatine Kinase (CK)](https://www.testing.com/tests/creatine-kinase-ck/) - Myositis which is inflammation of the muscles.
- [CRP](https://www.testing.com/tests/c-reactive-protein-crp/)

## Immune system

I am not sure how many of these tests are commercially available. In most countries, only a few among the below would be available. But many of these tests are available as part of IncellDX covid long hauler panel in US.

- Immunoglobin A
- SCD40L

Cytokine panel covers the following cytokines.

Tumor necrosis factor

- Tumor necrosis factor alpha

Haematopoietins

- Granulocyte/Macrophage Colony Stimulating Factor

Interleukins

- Interleukin-1
- Interleukin-2
- Interleukin-4
- Interleukin-6
- Interleukin-10
- Interleukin-13

Interferons

- Interferon Beta
- Interferon Gamma

Chemokines

- CCL2/Monocyte Chemoattractant Protein-1
- CCL3/Macrophage Inflammatory Protein-1Alpha
- CCL4/Macrophage Inflammatory Protein-1Beta
- CCL5/RANTES
- Interleukin-8

Others

- Vascular endothelial growth factor (VEGF)

## Cardiovascular

- Apolipoprotein A1 and B

## POTS

- [NASA 10 minute Lean test](https://www.healthrising.org/blog/2020/08/25/bateman-nasa-lean-test-chronic-fatigue-syndrome-orthostatic-intolerance/)
- [Head up Tilt table test](https://my.clevelandclinic.org/health/diagnostics/17043-tilt-table-test)

## AntiPhospholipid Syndrome

Refer [this write up](./coagulation/aps.md)

## Homocysteine metabolism

High levels of homocysteine aka [Hyperhomocysteinemia](https://www.healthline.com/health/homocysteine-levels) can also lead to clotting events. Discuss with your physician if this can be causing some of your symptoms.

- [Homocysteine test](https://www.testing.com/tests/homocysteine/) - check
- [Methyl Malonic Acid(MMA)](https://www.testing.com/tests/methylmalonic-acid/)
- [Folic Acid Test](https://www.testing.com/tests/folate-test/)
- [Vitamin B12](https://www.testing.com/tests/vitamin-b12-and-folate/)
- [MTFHR Gene mutation](https://www.testing.com/tests/mthfr-mutation/)

## Coagulation

This section lists the useful tests under coagulation profile. You dont need to do all of these exhaustive list of tests. **Discuss this aspect of things with your physician and they will be able to navigate you towards diagnosis.**

- [D-Dimer](https://www.testing.com/tests/d-dimer/)
- [Fibrinogen](https://www.testing.com/tests/fibrinogen/)
- [Prothromin Time and INR](https://www.testing.com/tests/prothrombin-time-and-international-normalized-ratio-ptinr/)
- [Activated Partial Thromoplastin Time](https://www.testing.com/tests/partial-thromboplastin-time-ptt-aptt/),
- [von Willebrand Factor](https://www.testing.com/tests/von-willebrand-factor/)

Below three tests are needed for detecting and treating **microclots**.

- [Thromboelastography(TEG)](https://en.wikipedia.org/wiki/Thromboelastography)
- [PFA200 aka Platelet Function Assay](https://www.testing.com/tests/platelet-function-tests/)

Tests to measure natural anticoagulants

- [Protein C and Protein S](https://www.testing.com/tests/protein-c-and-protein-s/)
- [Antithrombin](https://www.testing.com/tests/antithrombin/)

## Mast Cell Activation Syndrome(MCAS)

- [Tryptase](https://www.testing.com/tests/tryptase/)
- [Histamine](https://www.testing.com/tests/histamine/)

## Arthritis

- [Uric acid](https://www.testing.com/tests/uric-acid/)
- [Rheumatoid Factor](https://www.testing.com/tests/rheumatoid-factor-rf/)
- [Cyclic Citrullinated Peptide (CCP) Antibody](https://www.testing.com/tests/cyclic-citrullinated-peptide-antibody/)
- [Antinuclear Antibody](https://www.testing.com/tests/antinuclear-antibody-ana/)
- [Synovial Fluid Analysis](https://www.testing.com/tests/synovial-fluid-analysis/)
- [Psoriatic Arthritis](https://www.testing.com/psoriatic-arthritis-testing/)

## Ongoing Research

These tests are taken from ongoing research. These tests are not available yet as part of the standard suite of tests.

### Testing for Microclots

These are based on the biomarkers discussed in the paper [Persistent clotting protein pathology in Long COVID/Post-Acute Sequelae of COVID-19 (PASC) is accompanied by increased levels of antiplasmin](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8381139/). Patients with the symptoms like fatigue, brain fog, loss of concentration and forgetfulness, shortness of breath, as well as joint and muscle pains seem to test positive for microclots.

- [Microclot testing](./coagulation/microclots.md)
- Antiplasmin
- Plasminogen Activator Inhibitor 1

### GPCR Autoantibodies

As of now only available in 3 labs in Germany. [Checkout this link from Celltrend](https://www.celltrend.de/en/pots-cfs-me-sfn/). People with ME/CFS who had covid and then went on to develop Long covid seem to test positive for many of these auto antibodies. This is what I observed from people on twitter.

- Angiotensin-II-Receptor-1 (AT1R) IgG-auto-antibodies
- Endothelin-Receptor-A (ETAR) IgG-auto-antibodies
- Beta-1 adrenergic receptor auto-antibodies
- Beta-2 adrenergic receptor auto-antibodies
- Muscarinic cholinergic (M1) receptor auto-antibodies
- Muscarinic cholinergic (M2) receptor auto-antibodies
- Muscarinic cholinergic (M3) receptor auto-antibodies
- Muscarinic cholinergic (M4) receptor auto-antibodies
- Muscarinic cholinergic (M5) receptor auto-antibodies
- Alpha-1 adrenergic receptor auto-antibodies
- Alpha-2 adrenergic receptor auto-antibodies

### Other new tests

- Collagen Type II antibodies
- Natural killer cells functional
- Ganglionic ACHR antibody
