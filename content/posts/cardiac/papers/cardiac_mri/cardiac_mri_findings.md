# Heart findings in Vaccine related myocarditis

## [Cardiac MRI Findings in COVID-19 Vaccine-Related Myocarditis: A Pooled Analysis of 468 Patients][1]

- Lake Louise criteria for the diagnosis of myocarditis.

Cardiac MRI Findings

- Elevated T2
- Myocardial late gadolinium enhancement(LGE)
- Subepicardial and/or midwall pattern
- Impaired left ventricular ejection fraction in `4%`
- Elevated T1 and extracellular volume fraction (ECV)
- Pericardial effusion
- Wall motion abnormality

## [Cardiac MRI Findings of Myocarditis After COVID-19 mRNA Vaccination in Adolescents][2]

> All cardiac MRI examinations were performed using a 1.5-T magnet (Magnetom Sola, Siemens Healthcare). The protocol included the following sequences: axial and multiplanar cine precontrast SSFP, T1- and T2-weighted spectral adiabatic inversion recovery (SPAIR), perfusion imaging, early postcontrast short-axis SSFP, time inversion scout, and multiplanar T1-weighted inversion recovery segmented breath-hold late gadolinium enhancement (LGE) imaging.

> The LGE imaging was performed using IV administration of gadobenate dimeglumine. T1 and T2 parametric imaging was acquired at the discretion of the protocoling radiologist. For the scanner that performs cardiac MRI at our institution (University of Maryland Medical Center), normal reference native T1 parametric values were determined to be less than 1100 milliseconds, and normal T2 parametric values were determined to be less than 55 milliseconds.

## [MRI and CT coronary angiography in survivors of COVID-19][3]

This study includes hospitalized as well as ventilated patients and compares against 10 healthy young adults

- Reduced left ejection fraction
- Right ventricular systolic function with elevated native T1 values
- Extracellular volume fraction
- Reduced myocardial manganese uptake

Comparison with comorbid matched volunteers

- Left ventricular function preserved
- Reduced right ventricular systolic function but with comparable native T1 values
- Extracellular volume fraction
- Presence of late gadolinium enhancement and manganese uptake

**NOTE** - Findings remained irrespective of the disease severity.

Conclusion: Right ventricular dysfunction

## [Cardiac MRI and Myocardial Injury in COVID-19: Diagnosis, Risk Stratification and Prognosis][4]

> T2-weighted imaging, early gadolinium enhancement (EGE), and late gadolinium enhancement (LGE) signal intensities used to see pathophysiologic changes from edemas and hyperemia to fibrosis are coupled with quantitative parametric mapping of T1 and T2 relaxation times to improve diagnostic accuracy and identify both acute and chronic changes of myocardial inflammation.

> Clinical indicators of myocardial damage include elevated cardiac biomarkers such as troponin I, ST segment and T wave changes on electrocardiogram (ECG), and newly reduced left ventricular ejection fraction (LVEF) on cardiac imaging.

Various mechanisms of myocardial injury in COVID-19 have been proposed:

- direct viral injury causing myocarditis
- systemic inflammation secondary to cytokine storm
- stress-induced cardiomyopathy
- microvascular thrombosis

In the typical viral myocarditis pathophysiology, entry of viral particles in cardiac myocytes leads to an innate and humeral immune response and results in focal or diffuse myocardial necrosis. Within a few days of a direct cellular injury, edemas and necrosis can lead to clinical symptoms associated with adverse changes in myocardial contractile function, including heart failure and arrhythmia.

> **SARS-CoV-1** virus has shown the ability to persist in the myocardium for several weeks to months, but has also been shown to clear the myocardium in less than a week. It is also unclear if SARS-CoV-1 myocarditis would lead to chronically dilated cardiomyopathy as with other more viral etiologies of myocarditis such as adenovirus or enterovirus

This is very important

> Using the original 2009 LLC, myocarditis is typically visualized with T2-weighted imaging to target edemas, with early gadolinium enhancement (EGE) to target hyperemia and capillary leaks, and with late gadolinium enhancement (LGE) to detect scars and fibrosis, but also acute changes in the extracellular volume

---

[1]: https://onlinelibrary.wiley.com/doi/10.1002/jmri.28268 "Cardiac MRI Findings in COVID-19 Vaccine-Related Myocarditis: A Pooled Analysis of 468 Patients"
[2]: https://www.ajronline.org/doi/10.2214/AJR.21.26853 "Cardiac MRI Findings of Myocarditis After COVID-19 mRNA Vaccination in Adolescents"
[3]: https://heart.bmj.com/content/108/1/46 "MRI and CT coronary angiography in survivors of COVID-19"
[4]: https://www.mdpi.com/2075-4418/11/1/130/htm "Cardiac MRI and Myocardial Injury in COVID-19: Diagnosis, Risk Stratification and Prognosis"
