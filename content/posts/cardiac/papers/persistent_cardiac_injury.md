# Persistent cardiac injury – An important component of long COVID-19 syndrome

---

## References

- [Persistent cardiac injury – An important component of long COVID-19 syndrome](<https://www.thelancet.com/journals/ebiom/article/PIIS2352-3964(22)00076-7/fulltext>)
