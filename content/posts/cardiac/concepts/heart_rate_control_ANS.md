# Autonomic nervous system and heart rate control

- Sinoatrial Node(SAN) is the pacemaker of the heart responsible for the heart rate(chronotrophy). Without any external influence, SAN generate electrical impulses that causes the heart rate to be around 100bpm.
- Nerve impulses and hormones can influence the cells in SAN and affect the speed at which the SAN generates electrical impulses thereby controlling the heart rate.
- Negative chronotrophy - decreasing heart rate.

## Autonomic nervous system

- ANS induces force of contraction of the heart and its heart rate. It has sympathetic and parasympathetic divisions.

- Acetylcholine is the neurotransmitter for parasympathetic system while adrenaline(epinephrine, norepinephrine) is the neurotransmitter for the sympathetic nervous system.
- Cells have cholinergic and adrenergic receptors for the above neurotransmitters.

![ANS and the heart](innervation_of_heart_ans.jpg)

### Parasympathetic input

- Parasympathetic input to the heart is via vagus nerve.
- vagus nerve forms synapses with the postganglionic cells in SAN
- When the vagus nerve is stimulated, acetylcholine binds to the M2 receptors (we have M1-M5 cholinergic receptors which are GCPR receptors)
- This binding decreases the slope of the pacemaker potential and leads to negative chronotropic effect.
- Parasympathetic input dominates at rest and hence the lower heart rate at rest.

### Sympathetic input

- Sympathetic input is through the sympathetic trunk that innervates(supplies nerve) the SAN and AVN.
- postganglionic fibres release noradrenaline that binds to the B1 adrenoreceptors to increase the slope of the pacemaker potential.
- This leads to the positive chronotropic effect as well as increases the force of contraction.

## Baroreceptor reflex

- Baroreceptors are mechanoreceptors located in the carotid sinus and the aortic arch.
- These receptors are sensitive to stretch and tension of the arterial wall as well as the arterial pressure.
- These receptors communicate the arterial pressure changes to the medulla oblangata in the brainstem.
- Medullary centres in the brain are responsible for the output of ANS. They use the feedback from the baroreceptors to coordinate a response.

![Baroreceptors and homeostatis](baroreceptors_and_homeostatis.jpg)

## Hormones

- Adrenaline is released from the medulla of adrenal glands during stress events.
- This initiates the stress response(flight or fight) that includes increase in the HR.

---

## References

- [Control of Heart Rate](https://teachmephysiology.com/cardiovascular-system/cardiac-output/control-heart-rate/)
