# Elevated VEGF SCD40L

I am trying to see if I can connect the dots if the following cytokines are elevated.

- IL-4
- IL-8
- VEGF
- SCD40L
- IFN-gamma

## Terminologies

- Angiogenesis refers to the development of new blood vessels. It is a complex biological process mediated by endothelial cell (EC) proliferation, migration, and morphogenesis. Important in wound healing. Its dysregulation after a viral infection/injury to the vessel wall can result in atherosclerosis.

- Denuded artery - Artery with denudation of endothelium. Endothelial denudation - leads to endothelial cell dysfunction.

## Interpretation

Endothelial injury/damage/dysfunction covid happen post covid. We know VEGF stimulates endothelial growth and has angiogenic effect.

VEGF is released from activated platelets. VEGF should be measured from plasma(after centrifuging). Platelets activate because of endothelial damage, then it makes sense for them to release VEGF, so that endothelial cells proliferate and migrate to heal the injury.

Platelets up on activation release ~300 proteins. CD40L is also one such protein. Elevated SCD40L could also be a biomarker of activated platelets.

---

## References

- [Soluble Platelet Release Factors as Biomarkers for Cardiovascular Disease](https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC8255615/)
