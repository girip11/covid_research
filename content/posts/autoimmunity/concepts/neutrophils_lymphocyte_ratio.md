# Neutrophils to Lymphocytes ratio

A normal NLR is roughly 1-3. An NLR of 6-9 suggests mild stress (e.g. a patient with uncomplicated appendicitis). Critically ill patients will often have an NLR of `~9` or higher

[2] concluded "Patients aged ≥ 50 and having an NLR ≥ 3.13 are predicted to develop critical illness"

> Recent studies have reported that low lymphocyte-to-C-reactive protein ratio , platelet-to-lymphocyte ratio, and thrombocytopenia may be associated with critical illness.

## My takeaways

- Can NLR be used to predict incidence of long covid?

In my case, I developed long covid. I have the following stats

- Day 2 of testing positive - Neutrophils 73.2% and lymphocytes 11.2%. `NLR > 6`
- Day 7 of testing positive - Neutrophils 71% and lymphocytes 23%. `NLR > 3`
- Day 50 - Neutrophils 66.1 and lymphocytes 29.3. `NLR < 3`
- After 8 months - Neutrophils 66.1 and lymphocytes 28. `NLR < 3`

Interestingly there is so much that we can gain from just looking at the CBC test.

> RDW-SD levels may be a potent independent predictor of the infection severity and mortality probability in COVID-19 patients.

---

## References

1. []()
2. [Neutrophil-to-lymphocyte ratio predicts critical illness patients with 2019 coronavirus disease in the early stage](https://translational-medicine.biomedcentral.com/articles/10.1186/s12967-020-02374-0)
3. [NLR calculator](https://www.mdcalc.com/neutrophil-lymphocyte-ratio-nlr-calculator)
4. [Validation of red cell distribution width as a COVID-19 severity screening tool](https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC8056748/)
