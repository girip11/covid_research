# Cytokines

## Introduction

- For effective immune response, communication is required between lymphocytes(B-cells, T-cells), monocytes, inflammatory cells etc.
- Cytokines facilitate this communication between these immune cells.
- diverse but small glycoproteins
- influences both innate and adaptive immune responses.
- Two principle producers of cytokines are helper(CD4+) T-cells and macrophages, though they can transiently induced by virtually all nucleated cells.

## Cytokine and signalling cascade

![Cytokine stimulation](../images/cytokine_production.png)

- Cytokines bind to receptors present on the surface of the target cells.
- Cytokines can bind to the same cell(autocrine), nearby cell(paracrine) or distant cell(endocrine)
- Cytokine receptor engagement on the target cell triggers an intracellular signalling cascades resulting in either differentiation, proliferation and activation of the target cell.

## Cytokine interaction functionality types

Cytokine binding to a target cell can be either of the following.

- Pleiotropic - same cytokine different effect on different target cells.(Polymorphism)
- Redundant - multiple cytokines can have same effect
- Synergic - cooperative effect
- Antagonistic - inhibition of something
- Cascade induction - initiating a multi-step feedforward process

## Types

Cytokines belong to one of the below family.

- Interferon - Antiviral proteins
- Chemokine - direct cell migration, adhesion and activation
- Interleukin - cell type and interleukin specific actions.
- Tumor Necrosis factor - Inflammatory and immune response
- Haematopoietins - cell proliferation and differentiation
- Transforming growth factor - Regulation of immune cells

### Interferons

Cytokine family and are glycoproteins and released by cells in presence of pathogens.

- "interfere" with viral replication in host cells. Very important for fighting viral infections
- activate NK cells, macrophages
- Increase MHC expression and thereby up-regulate antigen presentation to T-cells
- Uninfected cells to resist antigen

Interferons divided into three classes Type I, II, and III

Type I interferons - `IFN-alpha`, `IFN-beta`, `IFN-omega`. binds to `IFN-alpha` receptor
Type II interferons - `IFN-alpha`, `IFN-Beta`, `IFN-gamma`. binds to `IFN-gamma` receptor.

Physically muscle aches and fever result from production of interferons. They can also inflame the tongue and cause taste bud cells to dysfunction.

### Chemokines

- family of chemoattractant cytokines

Chemokines play a vital role in

- Cell migration from blood into the tissue and viceversa(transendothelial migration)
- induction of cell movement (known as chemotaxis)
- regulate lymphoid organ development
- T-cell differentiation
- mediate tumour cell spread(metastasis)
- Neuromodulators

Cells express chemokine receptor. **Chemokine receptors belong to vast family of G-protein coupled receptors(GPCRs)**. Chemokine binding to its receptor initiates calcium signalling cascade.

CC and CXC are two largest groups of chemokines. Other two groups being CX3C and C. 47 known chemokines and 19 chemokine receptors. Molecules expressed on cell will determine which tissue the cell will migrate to.

Chemokines can be either inflammatory(leukocyte recruitement - chemoattractant) or homeostatic(leukocyte movement - chemotaxis).

- When inflammed tissues or infection sites release cytokines like IL-1 Tumour necrosis Factor (TNF), inflammatory chemokines are produced to recruit leukocytes(monocytes and neutrophils and other effector cells).
- Homeostatic chemokines play key role in lymphocyte migration to lymph nodes to screen for pathogens and development of lymphoid organs.

### Interleukin

- The majority of interleukins are synthesized by helper T-cells, monocytes, macrophages, and endothelial cells.
- They promote the development and differentiation of T and B lymphocytes, and hematopoietic cells.

### Tumor Necrosis factor

- Macrophages release TNF when they detect infection to alert other immune cells.
- TNFR1 and TNFR2 are the receptors.
- TNFR1 is expressed by most cells types, its proinflammatory and apoptotic
- TNFR2 restricted to endothelial, epithelial and subset of immune cells and its signalling is antiinflammatory, promotes cell proliferation and wound healing.
- is an endogenous pyrogen, is able to induce fever, apoptotic cell death, cachexia, and inflammation, inhibit tumorigenesis and viral replication, and respond to sepsis via IL-1 and IL-6-producing cells.

## Regulation

- Cytokine production needs to be regulated otherwise it can be detrimental.

---

## References

- [Cytokines](https://www.immunology.org/public-information/bitesized-immunology/receptors-and-molecules/cytokines-introduction)
- [Interferons](<https://bio.libretexts.org/Bookshelves/Microbiology/Book%3A_Microbiology_(Boundless)/11%3A_Immunology/11.04%3A_Innate_Defenders/11.4B%3A_Interferons>)
- [Chemokines](https://www.immunology.org/public-information/bitesized-immunology/receptors-and-molecules/chemokines-introduction)
- [Cytokines: Interleukins and chemokines](https://www.youtube.com/watch?v=jNqXOjd-TMI)
