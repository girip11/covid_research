# Mechanisms of Autoantibody-Induced Pathology

## Summary

Autoantibodies can be classified into the following categories:

1. mimic receptor stimulation
2. blocking of neural transmission
3. induction of altered signaling
4. triggering uncontrolled microthrombosis
5. cell lysis
6. neutrophil activation
7. induction of inflammation.

## Highlights

### Mimic receptor stimulation

- Thyroid stimulating autoantibodies

### Blockade of neural transmission by receptor blockade or alteration of the synaptic structures

- Antibodies against muscle nicotinic acetylcholine receptors in myastehnia gravis

### Induction of altered signaling

- antibodies against desmoglein-3 in pemphigus.

### Triggering uncontrolled microthrombosis

- autoantibodies against ADAMTS13

### Cell lysis

- antiplatelet autoantibodies leading to thrombocytopenia

### Uncontrolled neutrophil activation

- antineutrophil cytoplasmatic autoantibodies in granulomatosis with polyangitis.

### Induction of inflammation at the site of autoantibody binding

- autoantibodies against structural proteins of the skin in PD, or autoantibodies targeting myosin in myocarditis or autoantibodies recognizing citrullinated proteins in rheumatoid arthritis (RA) or autoantibodies targeting aquaporin-4 (AQP4) in neuromyelitis optica (NMO)

### Discussion

> B cells producing antibodies that bind with high affinity to self-antigens are efficiently eliminated or undergo anergy, while those **B cells that produce autoantibodies with medium or low binding affinity may escape, even in non-autoimmune individuals**. Many of these low-affinity autoantibodies are polyreactive and recognize self-antigens as well as pathogen-derived antigens
> IgG subclass and glycosylation/sialylation patterns determine whether an autoantibody exhibits FcγR-mediated pro-or anti-inflammatory functions.
> the cross talk between activated B and T lymphocytes seems to be crucial for the outcome of antibody responses and their pathogenic potential, i.e., the antibody class and glycosylation pattern

## References

- [Mechanisms of Autoantibody-Induced Pathology](https://www.frontiersin.org/articles/10.3389/fimmu.2017.00603/full)
