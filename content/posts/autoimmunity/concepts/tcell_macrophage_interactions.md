# T-cell macrophage interactions

- Inflammatory process is body's response against damage/disease
- Immune cells deal with antigen and repair tissue damage.
- T-cells and macrophages are two main immune cells.

- T-cells (Helper T-cells) play crucial role in recognizing antigen protein and initiating the immune response. Autoimmune condition occur when the T-cells recognizing human protein to be antigenic resulting in inflammation.

## Killer T-cells(Cytotoxic T-cells) and antigens

- As we know Killer T-cells(CD8+) recognize MHC class I.
- T-cell has receptors on its surface and each one can bind to one type of antigen.
- Killer T-cells learn to recognize antigen from the dendritic cells.
- IL-2 causes the killer T-cells to proliferate to more killer T-cells and memory T-cells.
- Killer T-cells destroy the antigen presenting infected cells.
- Virus infected cells will have pieces of virus antigens on the surface loaded on to the MHC class I molecule. This helps in identifying and killing by the Killer T-cells.
  Once T-cell receptor binds to the viral antigen on the infected cell's surface, it releases cytotoxins to kill that cell(apoptosis).
- Then the clean up of the dead cell is done by the macrophages.

## Dendritic cells

- Dendritic cells help activate helper T-cells and killer T-cells as well as B-cells by presenting them with antigens derived from the pathogen.
- Every helper T-cell is specific to one particular antigen.
- Only professional antigen-presenting cells (APCs: macrophages, B lymphocytes, and dendritic cells) are able to activate a resting helper T-cell when the matching antigen is presented.
- Dendritic cells can activate both memory and naive T cells, and are the most potent of all the antigen-presenting cells.

---

## References

- [T-cell macrophage interactions](https://www.bristol.ac.uk/cellmolmed/air/ourresearch/project.tcell_mp.html)
- [Killer T-cell activation and killing mechanism](https://www.youtube.com/watch?v=eOO8jEFsodo)
- [T-cell activation](https://www.immunology.org/public-information/bitesized-immunology/systems-and-processes/t-cell-activation)
