# Hypergammaglobulinemia and autoantibody induction mechanisms in viral infections

---

## References

- [Hypergammaglobulinemia and autoantibody induction mechanisms in viral infections](https://pubmed.ncbi.nlm.nih.gov/12627229/)
