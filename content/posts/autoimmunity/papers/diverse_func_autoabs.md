# Diverse functional autoantibodies in patients with COVID-19

---

## References

- [Diverse functional autoantibodies in patients with COVID-19](https://www.nature.com/articles/s41586-021-03631-y)
