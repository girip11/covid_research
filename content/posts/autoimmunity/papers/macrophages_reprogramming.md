# Long-lived macrophage reprogramming drives spike protein-mediated inflammasome activation in COVID-19

## Terminologies

- Prokaryotes - unicellular always but without nucleus, mitochondria. Ex- bacteria
- Humans are eukaryotes. Often eukaryotes are multicellular where each cell contains nucleus, mitochondria and other organelles bound by outer membrane.

- We know cells contains organelles like ribosome, mitochondria etc.
- Cytosol and cytoplasm are two different components of a cell. Cytoplasm is present within the cell membrane while Cytosol is the intracellular fluid present within the cytoplasm.
- The inner concentrated region of the cytoplasm is called the endoplasm, whereas, the outer region of the cytoplasm is called ectoplasm.

![Cytosol vs Cystoplasm](cytosol_and_cytoplasm.png)

### Inflammasome

- cytosolic multiprotein complexes(intracellular protein complexes) of the innate immune system responsible for the activation of inflammatory responses.
- 4 key inflammasome receptors are NLRP1, NLRP3, NLRC4, AIM2. These receptors activate inflammasomes on recognizing pathogen associated molecular pattern(PAMP/DAMP)
- Activated inflammasomes activate procaspase-1 which inturn induces secretion and cleavage of inflammatory proteins leading to release of proinflammatory cytokines `IL-1β`, `IL-18`
- Also plays a role in programmed cell death called pyroptosis where the cell releases its cytoplasmic content to induce proinflammatory signalling.
- `IL-1β` and `IL-18` released following inflammasome activation were found to induce `IFN-γ` secretion and natural killer cell activation.

- Immunogenicity - Ability of foreign substance to provoke immune response.

## Summary

- Spike(S) protein leads to inflammasome formation and subsequent cytokine release in macrophages derived from covid-19 patients.

- Our findings reveal that SARS-CoV-2 infection causes profound and long-lived reprogramming of macrophages resulting in augmented immunogenicity of the SARS-CoV-2 S-protein, a major vaccine antigen and potent driver of adaptive and innate immune signaling.

## Highlights

> Profound alteration of **macrophage gene activation and expression for several weeks to months after infection in both severe and mild COVID‐19 patients** can provide a better understanding of post‐COVID inflammatory syndromes.

---

## References

- [Long-lived macrophage reprogramming drives spike protein-mediated inflammasome activation in COVID-19](https://pubmed.ncbi.nlm.nih.gov/34133077/)
- [Inflammasome](https://en.wikipedia.org/wiki/Inflammasome)
- [Cytosol vs Cytoplasm](https://byjus.com/biology/difference-between-cytosol-and-cytoplasm/)
- [Immunogenicity](https://www.astrazeneca.com/what-science-can-do/topics/disease-understanding/what-does-immunogenicity-mean-in-the-context-of-covid-19-vaccines.html)
