# Persistence of SARS CoV-2 S1 Protein in CD16+ Monocytes in Post-Acute Sequelae of COVID-19 (PASC) up to 15 Months Post-Infection

## Theory

> The three subtypes of circulating monocytes (classical, intermediate, non-classical) express very different cell surface molecules and serve very different functions in the immune system. Generally, classical’ monocytes exhibit phagocytic activity, produce higher levels of ROS and secrete proinflammatory molecules such as IL-6, IL-8, CCL2, CCL3 and CCL5. Intermediate monocytes express the highest levels of CCR5 and are characterized by their antigen presentation capabilities, as well as the secretion of TNF-α, IL-1β, IL-6, and CCL3 upon TLR stimulations. Non-classical monocytes expressing high levels of CX3CR1 are involved in complement and Fc gamma-mediated phagocytosis and anti-viral responses
>
> In humans, 85% of the circulating monocyte pool are classical monocytes, whereas the remaining 15% consist of intermediate and non-classical monocytes
> Classical monocytes have a circulating lifespan of approximately one day before they either migrate into tissues, die, or turn into intermediate and subsequently non-classical monocytes

## Findings

> The levels of both intermediate (CD14+, CD16+) and non-classical monocyte (CD14-, CD16+) were significantly elevated in PASC patients up to 15 months post-acute infection compared to healthy controls.
> A statistically significant number of non-classical monocytes contained **SARS-CoV-2 S1 protein in both severe** (P=0.004) and PASC patients (P=0.02) out to 15 months post-infection
> only fragmented SARS-CoV-2 RNA was found in PASC patients. No full length sequences were identified, and no sequences that could account for the observed S1 protein were identified in any patient. That non-classical monocytes may be a source of inflammation in PASC warrants further study.

## Highlights

> The predominant immune cell abnormality was elevations in monocyte subsets
> Monocyte subpopulations are divided into 3 phenotypic and functionally distinct types. Classical monocytes exhibit the CD14++, CD16- phenotype, intermediate monocytes exhibit a CD14+, CD16+ phenotype, and the non-classical monocytes express CD14lo, CD16+
> In particular, classical monocytes express high levels of the ACE-2 receptor, the putative receptor for SARS-CoV-2 (8). Intermediate and non-classical monocytes express very little ACE-2 receptor. Similarly, classical monocytes express low levels of the chemokine receptors CX3R1 and CCR5. Intermediate monocytes express high levels of CCR5 while non-classical monocytes express high levels of CXC3R1.

### Non classical monocyte persistence

> It has been shown in both humans and mice that non-classical monocytes require fractalkine (CX3CL1) and TNF to inhibit apoptosis and promote their survival. Our previous data show high IFN-γ levels in PASC individuals (5), which can induce TNF-α production
> Further, TNF-α and IFN-γ induce CX3CL1/Fractalkine production by vascular endothelial cells creating the conditions that could promote survival of non-classical monocytes
> Interestingly, a number of papers have been written discussing the increased mobilization of CD14lo, CD16+ monocytes with exercise. These data could help to explain reports of worsening PASC symptoms in individuals resuming pre-COVID exercise regimens.
> It is important to note that the S1 protein detected in these patients appears to be retained from prior infection or phagocytosis of infected cells undergoing apoptosis and is not the result of persistent viral replication

## My takaways

- Intermediate and non-classical monocytes were elevated among the monocyte subsets. S1 subunit present in the non-classical monocytes only.

- What can be done to get rid of those circulating S1+ non classical monocytes.

---

## References

- [Persistence of SARS CoV-2 S1 Protein in CD16+ Monocytes in Post-Acute Sequelae of COVID-19 (PASC) up to 15 Months Post-Infection](https://www.frontiersin.org/articles/10.3389/fimmu.2021.746021/full)
