# COVID-19—A Theory of Autoimmunity Against ACE-2 Explained

It is established that the main receptor for viral entry into tissues is the protein **angiotensin-converting enzyme-2**(ACE-2)

The spike proteins represent the attachment point for entry of the virus into the cell.

ACE-2 is a Type 1 membrane-bound glycoprotein with 42% homology to angiotensin-converting enzyme (“ACE”) (14). The primary purpose of ACE-2 is to convert angiotensin II to angiotensin (1–7) which has an impact on arterial pressures and inflammation

## Soluble ACE2

Soluble ACE-2, also called serum or plasma ACE-2, refers to the ACE-2 enzyme that has been cleaved from the cell surface, a process called shedding. Shedding seems to occur more frequently in hypertension and heart disease

It is strongly predicted that SARS-CoV-2 can not only bind to soluble ACE-2, but also suggest that the strength of binding might be negatively impacted by **nicotine, a finding which is being considered for potential therapeutic value.**

The assumption that soluble ACE-2 binds SARS-CoV-2 is central to the hypothesis of autoimmunity to ACE-2. By testing in experimental setup, the researchers were able to confirm that a complex of SARS-CoV-2 and soluble ACE-2 is formed and circulating in the blood of infected patients.

[This paper discusses measuring soluble ACE2 in the plasma and using it as a biomarker][1]

## Hypothesis

> **There appears to be genetic polymorphism of the ACE-2 with increased risk of specific comorbidities—hypertension, cardiovascular disease, and diabetes**

**NOTE**- My father has all the above mentioned BP, CVD, diabetes.

The current hypothesis of autoimmunity postulates that higher levels of soluble ACE-2, or augmented conformational binding to the spike protein, increases the probability that the combined entity will be processed by an antigen-presenting cell as part of the virus. This may lead to antibody production against ACE-2, which triggers Type 2 and 3 hypersensitivity responses, and Type 4 cellular immune targeting after the viral particles with attached soluble ACE-2 are processed by antigen-presenting cells.

There are undetectable levels in the serum of healthy individuals and **a correlation exists between the occurrence of soluble ACE-2 and an individual's age**. Recent research has indicated that soluble ACE-2 is the most significant risk factor for cardiometabolic mortality and could be relevant in COVID-19).

## Proposal and Finding

It has been proposed that autoimmunity to ACE-2 and ACE is triggered by the viral infection in SARS-COV-2.

It was postulated that in people with high levels of soluble ACE-2, the viral spike protein binding tightly together with ACE-2 could be endocytosed by macrophages which would function as antigen-presenting cells. The whole viral protein could be proteolytically cleaved, including ACE-2, tightly attached to the viral spike protein, possibly producing new epitopes for antibody generation.

ACE-2 has a 42% homology with ACE, which could mean that autoantibodies that were formed against ACE-2 might cross- react with ACE as well. In this circumstance, the antibodies would target both ACE and ACE-2 cellular attached enzymes, leading to severe inflammation throughout the body, especially on lung endothelial cells.

**Consistent with this hypothesis, very recent data documented higher levels of soluble ACE-2 in the serum of critically ill CoVid-19 patients**.

## Conclusion

This theory of autoimmunity in SARS-COV-2 has been reinforced by the recent RECOVERY Trial showing the benefit of the steroid dexamethasone for immunosuppression.

There appears to be a benefit which would support the hypothesis of immune dysregulation due to autoimmunity to ACE and ACE-2.

---

## References

- [COVID-19—A Theory of Autoimmunity Against ACE-2 Explained](https://www.frontiersin.org/articles/10.3389/fimmu.2021.582166/full)

[1]: https://www.news-medical.net/news/20210713/Blood-test-could-be-a-simple-and-effective-method-for-monitoring-SARS-CoV-2-infection.aspx "Blood test could be a simple and effective method for monitoring SARS-CoV-2 infection"
