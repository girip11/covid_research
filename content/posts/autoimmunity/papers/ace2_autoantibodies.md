# Development of ACE2 autoantibodies after SARS-CoV-2 infection

---

## References

- [Development of ACE2 autoantibodies after SARS-CoV-2 infection](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0257016)
