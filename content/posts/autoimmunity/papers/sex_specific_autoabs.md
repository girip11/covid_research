# Paradoxical sex-specific patterns of autoantibody response to SARS-CoV-2 infection

---

## References

- [Paradoxical sex-specific patterns of autoantibody response to SARS-CoV-2 infection][1]

[1]: https://translational-medicine.biomedcentral.com/articles/10.1186/s12967-021-03184-8
