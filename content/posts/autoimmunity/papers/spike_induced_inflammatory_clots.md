# SARS-CoV-2 spike protein induces abnormal inflammatory blood clots neutralized by fibrin immunotherapy

## Summary

> SARS-CoV-2 spike induces structurally abnormal blood clots and thromboinflammation **neutralized by a fibrin-targeting antibody**.

## Goal

> We set out to determine how blood clots form in COVID-19 and to identify therapies to combat the deleterious effects of abnormal coagulation occurring in acute and convalescent stages of disease.

## Detailed notes

> - Spike protein from severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2) binds to the blood coagulation factor fibrinogen and induces structurally abnormal blood clots with heightened proinflammatory activity.

> - Persistent clotting pathology is prevalent in post-acute sequelae of SARS-CoV-2 infection (PASC, Long COVID)

> Overall, these results reveal an unanticipated role for SARS-CoV-2 Spike as a fibrinogen binding protein that alone accelerates the formation of abnormal clots with altered structure and increased inflammatory activity.

> Fibrin autoantibodies were abundant in all three groups of COVID-19 patients and persisted during the convalescent stage, but were scarce in healthy donor controls or in subjects with non-COVID respiratory illnesses

## Key terminologies

Microglia are a type of neuroglia (glial cell) located throughout **the brain and spinal cord**. Microglia account for 10–15% of all cells found within the brain. As the resident macrophage cells, they act as the first and main form of active immune defense in the central nervous system (CNS).

[Thromboinflammation][2] - Interplay between inflammation and coagulation

---

## References

- [SARS-CoV-2 spike protein induces abnormal inflammatory blood clots neutralized by fibrin immunotherapy](https://www.biorxiv.org/content/10.1101/2021.10.12.464152v1.full)

[2]: https://onlinelibrary.wiley.com/doi/10.1111/jth.14849 "Thromboinflammation and the hypercoagulability of COVID-19"
