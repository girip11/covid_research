# The Long Haul of COVID-19 Recovery: Immune Rejuvenation versus Immune Support

## Highlights

> Our findings indicated these debilitating issues(CFS) appeared to be associated with induced mitochondrial defects in biochemical energy production, and that this state of impairment could have a lasting deleterious impact on immune function.

### Nutritional recommendations

> This very helpful review provided insights into the daily dietary intakes of specific nutrients that support immune system function. These include vitamin A, vitamin C, vitamin D, vitamin E, and zinc, as well as omega-3 fatty acids and specific probiotic organisms.
> Based on my past experience with chronic fatigue syndrome, advanced medical nutrition therapy may be required in this situation.
>
> Immunosenescence is a term used to describe the aging of the immune system. It is now known that infection with the virus can accelerate this process and cause damage to immune cells.

### Immune damage

> It is now recognized that infection with the SARS-CoV-2 virus results in injury to the immune system and a residual “memory” of the infection.
> It has been demonstrated that this situation can create “bystander” damage to hematopoietic stem cells (the cells from which all immune cell types are produced)

### Immune rejuvenation and Autophagy

> This perspective indicates that an objective for improving immune function should be focused on reducing the production rate of damaged immune cells, the elimination of immune cells that carry messages from past exposures, and the replacement of those cells with new immune cells not imprinted with those memories. In other words: immune rejuvenation.
>
> This year—2020—Leo Swadling, PhD, discovered a population of T cells in the liver that can switch on autophagy to renew organelles like mitochondria to maintain their fitness
>
> It has now been demonstrated that autophagy is clinically connected to the activation of immune rejuvenation through the application of fasting physiology, as well as the consumption of diets that are low in refined starch and sugar, high in omega-3 fatty acids, and high in specific dietary polyphenols and flavonoids
>
> In addition, there is evidence that specific prebiotic and probiotic formulations may be important in supporting immune autophagy

### Immuno-Rejuvenation vs Immune Support

![Immune rejuvenation vs immune support](immune_support_vs_rejuvenation.png)

## My takeaways

- Autophagy via fasting and diets that are low in refined starch and sugar, high in omega-3 fatty acids, and high in specific dietary polyphenols and flavonoids

---

## References

- [The Long Haul of COVID-19 Recovery: Immune Rejuvenation versus Immune Support](https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC7819497/)
