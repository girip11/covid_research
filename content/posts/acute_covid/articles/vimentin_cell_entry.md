# Identifying a new protein that enables SARS-CoV-2 access into cells

- Extracellular vimentin can be facilitating the virus cell entry.
- Vimentin is expressed by all the cells of mesenchymal origin.

> More importantly, we saw that the CR3022 antibody inhibited the binding of vimentin with S protein, and neutralized SARS-CoV-2 entry into human cells

---

## References

- [Identifying a new protein that enables SARS-CoV-2 access into cells](https://medicalxpress.com/news/2022-01-protein-enables-sars-cov-access-cells.html)
