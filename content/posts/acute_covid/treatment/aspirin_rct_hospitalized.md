# Aspirin in patients admitted to hospital with COVID-19 (RECOVERY): a randomised, controlled, open-label, platform trial

- Safety and efficacy of Aspirin on hospitalized covid patients
- Aspirin anti-inflammatory and anti-thrombotic properties.

## Highlights

- 150mg Aspirin once a day + usual standard of care
- No change in mortality rate
- Small reduction in the discharge time
- Small reduction in the thrombotic events while small increase in the major bleeding events.

## My takeaway

Even with antiplatelet, mortality rate was unaffected. This might mean the following

- Otherways platelets can still be activated. So drugs might be required to address those pathways
- Anticoagulant might also be required in addition to antiplatelet. Fro instance, Nebulized heparin has shown some promise. Anticoagulant and antiplatelet combo has been recommended by Doctor Jaco Laubscher and its been his clinical practice.

If we can use afucosylation in antispike-antibodies in the early phase of the infection as a biomarker, may be those patients can only be treated with blood thinning meds?

What path activates the platelets needs to be found out, so that such pathways can be targeted.

---

## References

- [Aspirin in patients admitted to hospital with COVID-19 (RECOVERY): a randomised, controlled, open-label, platform trial](<https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(21)01825-0/fulltext>)
