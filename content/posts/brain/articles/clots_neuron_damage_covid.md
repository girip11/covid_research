# From blood clots to infected neurons, how COVID threatens the brain

## Issues

- Memory problems
- Difficulty concentrating
- Mental fog
- Mood changes

## Highlights

- 13% of hospitalized went on to develop neurological problems. 50% of them continued to experience the symptoms post 6 months while the other 50% improved.

> The current catalog of COVID-related threats to the brain includes bleeding, blood clots, inflammation, oxygen deprivation and disruption of the protective blood-brain barrier.

- In monkeys, neurons were affected. In the monkeys, the infection appeared to start with neurons connected to the nose and spread to other areas within a week.

### Entry to Brain

1. Olfactory bulb(nerve) on top of our nose is a potential route for virus to reach the brain from the respiratory system.

2. The blood-brain barrier (BBB) is composed of specialized brain microvascular endothelial cells (BMECs) that help regulate the flow of substances into and out of the brain. Injury to these endothelial cells (Antibody Dependent Cellular Cytotoxity) means the selective permeability is affected. So anything in the blood (including the virus) could leak in to the brain.

### Brain damage causes

1. Virus could directly affect the neurons
2. Lung hypoxia could also cause damage to the brain by depriving it of oxygen.

> - People have seen the virus inside of brain tissue. However, the viral particles in the brain tissue are not next to where there is injury or damage.
> - Its important to "treat that person early in the disease rather than when the disease has advanced so much that it has created damage that cannot be reversed

---

## References

- [From blood clots to infected neurons, how COVID threatens the brain](https://www.npr.org/sections/health-shots/2021/12/16/1064594686/how-covid-threatens-the-brain)

- [Elevation of Neurodegenerative Serum Biomarkers among Hospitalized COVID-19 Patients](https://www.medrxiv.org/content/10.1101/2021.09.01.21262985v1)
