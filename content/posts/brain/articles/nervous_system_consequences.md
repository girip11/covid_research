# Nervous system consequences of COVID-19

## Common Patient reported symptoms

- Anosmia/Parosmia
- Stroke
- Delirium
- Brain inflammation
- encephalopathy
- peripheral nerve syndromes

## Highlights

> Cerebrovascular complications co-occur with or even predate the onset of respiratory symptoms
> Central inflammatory and peripheral nerve conditions manifest on average 2 weeks later, suggesting that they may result from peri- or postinfectious processes

The above statement tries to conver vascular first respiratory secondary.

### Immune cells in the CSF

> Analysis of cerebrospinal fluid (CSF) from living patients with neuropsychiatric manifestations has almost uniformly failed to detect viral RNA by reverse transcription polymerase chain reaction.

Virus invasion into CSF is ruled out. So it is most likely immune activation and the inflammation in CNS. Basically viral encephalitis is ruled out.

> CSF shows up-regulation in the expression of interferon-regulated genes in dendritic cells(monocytes), along with activated T cells and natural killer (NK) cells. This is accompanied with an increase in interleukin-1 (IL-1) and IL-12, which is **not seen in blood plasma**

All these activated immune cells are capable of expressing CD16A receptor which has been known to be activated by the afucosylated antispike-antibody immune complex.

> During this acute phase, other markers of monocyte activation and neuronal injury can also be detected in CSF. In the following subacute phase, **patients with severe manifestations** show diminished interferon responses and markers of T cell exhaustion in CSF
>
> Autopsy studies of patients with acute COVID-19 show infiltration of macrophages, CD8+ T lymphocytes in perivascular regions, and widespread microglial activation throughout the brain
>
> The robust, generalized, and SARS-CoV-2–specific immune responses observed in the CNS are puzzling in the absence of readily detectable virus and may suggest transient infection of the brain very early in infection or low concentrations of viral antigen in the CNS

### Vascular dysfunction

> It is plausible that subtle forms of generalized vascular dysfunction, including thrombotic **microangiopathy** (microscopic blood clots) in the brain, may lead to neurological symptoms even in the absence of clinically apparent stroke.
>
> Additionally, high-field magnetic resonance examination of brain tissue demonstrates **microvascular damage in structures** plausibly related to neurologic manifestations of COVID-19, consistent with endothelial activation and widespread vascular injury observed in other organs.

### Neurological symptoms in Long Covid

> Many people who experience neurologic symptoms that linger after acute COVID-19 are less than 50 years old and were healthy and active prior to infection. Notably, the majority were never hospitalized during their acute COVID-19 illness, reflecting mild initial disease.
>
> Studies of positron emission tomography (PET) imaging also show decreased metabolic activity in the brain in people with Long Covid.
>
> Observations of neuroinflammation and neuronal injury in acute COVID-19 have raised the possibility that infection may accelerate or trigger future development of neurodegenerative diseases such as Alzheimer’s or Parkinson’s diseases.

---

## References

- [Nervous system consequences of COVID-19](https://www.science.org/doi/full/10.1126/science.abm2052)
