# Blood Vessel Attack May Spark Brain Inflammation in Long COVID (and ME/CFS)

## Microvascular Injury in the Brains of Patients with Covid-19

- This study highlighted small blood vessel damage and leakage across the brain.
- Found widespread punctuate hyperintensities.
- Proteins such as fibrinogen, immune complexes have leaked into the brain triggering an immune reaction.
- Possible damage to the blood brain barrier.

## Neurovascular injury with complement activation and inflammation in COVID-19

- All participants in this study died quite quickly after getting infected.
- Immune complexes were found on endothelial cells which line the blood vessels indicating an immune reaction.
- Antibodies and platelet aggregates were associated with proteins that are usuall produced by the classical pathway from the complement activation(immune system).
- Classical pathway activation is associated with autoimmune and inflammatory disorders.
- Damage to endothelial cells - causes leakage
- Increased levels of vonwillebrand factor - indicating damage to the endothelial cells. Hence the platelet activation and adhesion to the endothelial cells.

> An inflammatory response primarily involving the macrophages in the spaces surrounding the blood vessels reflected an attempt to repair the damage produced by the leaky blood vessels. With that, you have an ongoing inflammatory process taking place in the brain.

- Widespread neuron loss and damage in the brainstem. **No sign of infection in the brainstem.**

![Brain Covid Endothelial cells damage](Nath-endothelial-damage-brain-COVID-2022.jpg)

- The symptoms each person experiences depends on which part of the brain the blood vessel leakage occurs.

---

## References

- [Blood Vessel Attack May Spark Brain Inflammation in Long COVID (and ME/CFS)](https://www.healthrising.org/blog/2022/07/16/endothelial-cells-brain-inflammation-long-covid/)
- [Microvascular Injury in the Brains of Patients with Covid-19](https://pubmed.ncbi.nlm.nih.gov/33378608/)
- [Neurovascular injury with complement activation and inflammation in COVID-19](https://www.healthrising.org/blog/2022/07/16/endothelial-cells-brain-inflammation-long-covid/)
- [The classical pathway](https://www.creative-biolabs.com/complement-therapeutics/classical-pathway.htm)
