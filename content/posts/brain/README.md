# Covid and the Brain

## How covid affects the brain

1. Virus directly affecting the brain.
2. Inflammatory cytokines in the CSF affecting the brain
3. Lung hypoxia affecting the brain
4. Clots in the brain.
5. Endothelial damage in the microcapillaries in the brain
6. Autoimmunity

## Long covid symptoms

1. Cognitive issues (memory, concentration)
2. Insomnia
3. Headache
4. Mood changes
