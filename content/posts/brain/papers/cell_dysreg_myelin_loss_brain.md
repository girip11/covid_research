# Mild respiratory SARS-CoV-2 infection can cause multi-lineage cellular dysregulation and myelin loss in the brain

---

## References

- [Mild respiratory SARS-CoV-2 infection can cause multi-lineage cellular dysregulation and myelin loss in the brain](https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC8764721/)
