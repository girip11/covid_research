# Folic acid and cognitive impairment

## [Effects of folic acid supplementation on cognitive function and Aβ-related biomarkers in mild cognitive impairment: a randomized controlled trial][1]

- Low folate - poor cognitive performance
- Explore the effect of cognitive function with folic acid supplementation for 24 months.

Experiment

- Folic acid (400micrograms a day) given to the experiment group and controls did receive conventional treatment
- Blood AB related markers measured at 6, 12, 18 and 24 months.

Results

- Cognitive function improved.
- In the intervention group, homocysteine, S-adenosylhomocysteine , AB-42 and the expression of APP-mRNA were decreased and the expression of DNA methyltransferase mRNA were increased.

## [Effects of 6-Month Folic Acid Supplementation on Cognitive Function and Blood Biomarkers in Mild Cognitive Impairment: A Randomized Controlled Trial in China][2]

- Study the effect of folic acid on cognitive function in older adults.

Study

- RCT lasting 6 months
- 180 adults aged 65 yeats with mild cognitive impairment assigned to two groups
- First group with 400microgram of folic acid a day and the other group received conventional treatment
- Biomarkers and cognitive performance measured at baseline, 3 months and 6 months.

Results

- Improved folate, B12 levels, homocysteine and S-adenosylmethionine in the intervention group compared to controls.
- Improved IQ, Digit Span, Block Design scores at 6 months.

## [Folic Acid Treatment for Patients With Vascular Cognitive Impairment: A Systematic Review and Meta-Analysis][3]

- Vascular cognitive impairment
- Homocysteine as a contributor to the pathomechanisms involved in the cognitive impairment.
- Folic acid studies that were RCT were only considered for the meta analysis.

Conclusion

- Homocysteine levels were reduced.
- But improvement in the cognitive function wasnt significant.

---

## References

- [Effects of folic acid supplementation on cognitive function and Aβ-related biomarkers in mild cognitive impairment: a randomized controlled trial][1]

- [Effects of 6-Month Folic Acid Supplementation on Cognitive Function and Blood Biomarkers in Mild Cognitive Impairment: A Randomized Controlled Trial in China][2]

- [Folic Acid Treatment for Patients With Vascular Cognitive Impairment: A Systematic Review and Meta-Analysis][3]

[1]: https://pubmed.ncbi.nlm.nih.gov/29255930/
[2]: https://academic.oup.com/biomedgerontology/article/71/10/1376/2198033
[3]: https://academic.oup.com/ijnp/article/25/2/136/6428560
