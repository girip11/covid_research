# Neuroinflammation in Long covid Assessed by PET

## Abstract

- Study the relationship between neuroinflammation and circulating markers of the vascular dysfunction.

- Neuroinflammation found in various regions of the brain including **midcingulate and anterior cingulate cortex, corpus callosum, thalamus, basal ganglia, and at the boundaries of ventricles.**

## What the previous studies have found?

- Risk of hemorrhagic stroke and cerebral venous thrombosis doubled in the year following covid infection.
- Pre and post covid structural imaging of the brain of the same persons showed small but significant reduction in the grey matter and whole brain volume, increased markers of tissue damage
- Arterial spin labelling fMRI found reduced neurovascular perfusion.

## Glial cell activation

- Glial cells are innate immune cells. They have various activation states.
- Paracrine inflammatory mediators(proinflammatory cytokines), **pathogen-associated molecular patterns(PAMPs)**, damage associated molecular pattersn(DAMPs)
- Short term activation to clear pathogen and debris.
- Glial activation - disrupted cognition, increased pain, symptoms of malaise.

> Long-term glial activation can disrupt the delicate symbiosis of glia, blood vessels, neurons,
> and cerebrospinal fluid in normal central nervous system function.

- Neuroinflammation - activated glial cells, activated peripheral immune cells penetrating into the brain, brain endothelial cells inflammatory activation etc

## PET results with blood analyte measures

Positive pearson correlation between PET signal and the following vascular factors

- Fibrinogen (r=.80, p=.0032)
- α2-macroglobulin (r=.73, p=.011)
- orosomucoid (alpha-1-acid glycoprotein or AGP) (r=.69, p=.019)
- fetuin A (r=.68, p=.022)
- sL-selectin (soluble leukocyte selectin, or sCD62L) (r=.70, p=.025)
- pentraxin-2 (serum amyloid P component SAP) (r=.66, p=.026)
- haptoglobin (r=.74, p=.015)

CRP levels non significant to the PET signal.

All participants reported 5 out of 10 severity or greater on at least two of the eight ICC Neurological symptoms cluster items:

- headache
- unrefreshing sleep
- significant pain
- short-term memory loss
- difficulty
- processing information
- sensory sensitivity
- disturbed sleep patternand
- motor symptoms (e.g., twitching, poor coordination)

> Related to neuroinflammation, fibrinogen persistently activates glia at the glymphatic perivascular spaces that surround neurovasculature. Activated perivascular glia attract both glia from within the brain parenchyma and attract circulating immune factors to cross from neurovascular blood into brain.
> L-selectin (CD62L) is an adhesion molecule involved in attaching leukocytes (white blood cells) to vascular endothelium at sites of inflammation.
> Attraction of activated immune cells, inflammation of vascular endothelium, mobilization of glia, and activation of glia would each increase TSPO concentrations and therefore PET signal
> These vascular factors may penetrate into brain parenchyma via the perivascular
spaces that line the neurovasculature and form the blood-brain barrier.
> During neuroinflammation, the blood-brain barrier becomes more permissive by opening up at the perivascular spaces, allowing circulating factors to affect the brain's glia

- Increased PET signal found in the left lentiform nucleus of the basal ganglia. Basal ganglia contains many small but thin blood vessels. This is a common site of microhemorrhages.
- Enlarged perivascular spaces based on a study was associated with sleep dysfunction.
- Circumventricular organs (CVOs) are dense with ACE2 receptors, highly vascularized, feature fenestrated blood vessels that lack a complete BBB.

## References

- [Neuroinflammation in post-acute sequelae of COVID-19 (PASC) as assessed by [11C]PBR28 PET correlates with vascular disease measures](https://www.biorxiv.org/content/10.1101/2023.10.19.563117v1)
