# The cerebral network of COVID-19-related encephalopathy: a longitudinal voxel-based 18F-FDG-PET study

- Covid related encephalopathy using 18F-FDG-PET CT
- 7 patients were studied using PET CT during acute covid, 1 month and 6 month mark.
- SARS COV2 RT-PCR in CSF was negative. MRI revealed no specific abnormalities.
- Hypometabolism in the cerebral network of frontal cortex, anterior cingulate, insula and caudate nucleus.
- Patients clinically improved but cognitive and emotional disorders of varying severity remained.
- Prefrontal, insular and subcortical abnormalities lasted.

---

## References

- [The cerebral network of COVID-19-related encephalopathy: a longitudinal voxel-based 18F-FDG-PET study](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7810428/)
