# Difference Between Muscular Tissue and Nervous Tissue

- Muscular tissues specialized for contraction while nervous tissues specialized for communication.

## Tissue types

1. Epithelial
2. Muscular- responsible for locomotion and made of muscle fibres
   - Skeletal muscle
   - Cardiac muscle
   - Smooth muscle
3. Nervous - responsible for receiving signals and communication. Consists of neurons and glial cells.
   - Central nervous system
   - Peripheral nervous system
4. Connective

## Muscular tissues

### Skeletal muscles

- most abundant and is composed of skeletal muscle cells.
- Skeletal muscles attached with the skeleton via tendons.
- works voluntarily

### Cardiac muscles

- Present in heart walls
- works involuntarily

### Smooth muscles

- composed of smooth muscle cells.
- Present in organs, blood vessels, bronchioles of respiratory tract
- not striated and works involuntarily

## Nervous tissue

- Nerve cells aka neurons make the nervous tissue
- present in brain, spinal cord and peripheral nerves.
- Sensory, motor and associated nerve cells.

Sensory nerves - responsible for sensing the stimuli and signalling the central nervous system.

Motor nerve cells - transmit information from CNS directly to the organs.

Associated or intermediate nerve cells facilitate communication between the sensory and motor nerve cells.

---

## References

- [Difference Between Muscular Tissue and Nervous Tissue](https://www.differencebetween.com/difference-between-muscular-tissue-and-nervous-tissue/)
