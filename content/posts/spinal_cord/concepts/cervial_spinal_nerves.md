# Anatomy, Head and Neck, Cervical Nerves

- Cervical nerves arise from the cervical spine region of the nerves.
- They are responsible for carrying communication impulses to and from CNS.
- 8 pairs of cervical nerves through C1 to C8.
- These nerves are responsible for sensory and motor functions of head, neck, shoulders upper limbs and diaphragm.

## Structure and function

- Cervical nerves originates from the cervical spine in the form of rootlets.
- In each spinal nerve, the anterior and posterior root joins to form the complete nerve.

![Spinal nerve](spinal_nerve.jpg)

- After branching out of the spinal cord, the cervical nerves form the cervical and brachial plexuses.

### Cervial plexus

- Cervical plexus is formed from the ventral rami of C1-C4 and it gives rise to the motor and sensory branches.

![Cervical plexus](cervical_plexus.jpg)

#### Cervical plexus motor functions

- C1 nerve gives rise to two nerves that function in the oropharynx(middle of the throat)
- Hyoid bone is a U shaped bone in the neck that supports the tongue.
- Ansa cervicalis (loop of nerves) originate from C1-C3. The branches of these loop of nerves connect to the muscles the function to depress the hyoid bone which is necessary for **speech and swallowing**.
- C3-C5 roots form the phrenic nerves responsible for motor and sensory supply of diaphragm and symphatetic output.
- Several neck muscles(rectus capitis anterior/lateralis, longus capitis, Trapezius (i.e) triangle shaped muscles) are innervated to upper cervical nerve roots outside of cervical plexus (direct connection).

#### Cervical plexus sensory functions

- The cutaneous branches of the cervical plexus transmit sensory information from the skin of the neck, the superior portions of the thorax, and the scalp

- **Lesser Occipital nerve** arises from C2 and supplies the skin of the neck and the scalp.
- **Greater auricular nerve** of C2 and C3 supplies the skin over the parotid gland, the mastoid process, and the skin from the parotid gland to the mastoid process.
- **Transverse cervical nerve**, of C2 and C3 supply the skin of the anterior cervical region.
- **Supraclavicular nerve**, of C3 and C4 provides sensory information from the skin over the clavicle and shoulder.

### Brachial plexus

- The brachial plexus forms from the ventral rami of C5 to C8, as well as T1.
- The plexus is divided into several sections through its several anastomoses from the five nerve roots(C5, C6, C7, C8, T1) into three trunks, six divisions, three cords, and finally, five branches

![Brachial plexus nerves](brachial_plexus.jpeg)

Below sections are explanation of the above image.

Brachial plexus supplies the muscles on the upper body.

![Human muscles](human_muscles.jpg)

#### Roots

- Dorsal scapular nerve from C4 and C5 supplies to rhomboids and levator scapulae
- Phrenic nerve from C3-C5 supplies to diaphragm
- Long thoracic nerve from C5, C6 and C7 responsible for controlling serratus anterior.

#### Trunks

The three trunks are upper, middle and lower trunk.

Upper trunk of C5-C6 we have the following nerves

- Subclavius nerve that supplies subclavian muscles.
  ![Subclavius muscles](subclavius_muscle.jpg)

- Suprascapular nerve that supplies supra- and infraspinatus muscles.
  ![Supra and Infra spinatus muscles](supra_infra_spinatus_muscles.jpg)

#### Cords

The three cords are lateral, posterior and medial cord.

- Lateral pectoral nerve from the lateral cord
- Posterior cord gives rise to upper, lower subscapular nerves that supply the the upper and lower portions of subscapularis. Lower subscapular nerve also supplies the teres major and thoracodorsal nerve innervates the latissimus dorsi
- Medial pectoral nerve from the medial cord

Lateral and medial pectoral nerves supply the pectoralis major and minor muscles.

#### Branches

- Musculocutaneous nerve from the lateral cord supply the coracobrachialis, brachialis and biceps brachii. The musculocutaneous nerve provides sensation to the skin of the lateral forearm.
- Axillary nerve from posterior cord supplies the motor function of the deltaoid and teres minor as well as the sensory aspect of the overlying skin, the superior lateral cutaneous nerve of the arm and the skin of the lateral should and arm.
- Radial nerve from posterior cord is responsible for the motor innervation of the triceps brachii, the supinator, the anconeus, the brachioradialis, and all extensor muscles of the forearm. It also supplies the skin of the posterior arm, posterior hand, and the webbing between the thumb and index finger.
- Median nerve from lateral and medial cord controls most forearm flexors, except flexor carpi ulnaris and the medial portion of flexor digitorum profundus, the first and second lumbricals and the muscles of the thenar eminence. This nerve also provides sensation to the palmar aspect of the thumb, index finger, middle finger, and the lateral aspect of the fourth finger.
- Ulnar nerve from medial cord provides motor control to the flexor carpi ulnaris, the medial belly of flexor digitorum profundus, the two medial lumbricals, the interossei, and the muscles of the hypothenar eminence. It also provides sensation to the fifth digit, the hypothenar eminence, and the medial half of the fourth finger.

Apart from the above 5, we also have medical cutaneous nerve of forearm and arm rising from the medial cord. The medial cutaneous nerves of the arm and forearm are purely sensory and supply the medial skin of arm and forearm, respectively.

![Brachial plexus complete structure](brachial_plexus_skeletal_structure.png)

---

## Terminologies

- Innervated - supply part of the body(organ, muscle) with nerves.

## References

- [Anatomy, Head and Neck, Cervical Nerves](https://www.ncbi.nlm.nih.gov/books/NBK538136/)
