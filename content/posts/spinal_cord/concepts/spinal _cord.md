# Spinal cord

- Spinal cord's function is to conduct impulses from the brain to the body and generating the reflexes.

- It's part of the central nervous system
- Continuation of the brain stem.

## Segments

5 Segments. Each segment pairs of spinal nerves.

- Cervical - 8 pairs (C1-C8)
- Thoracic - 12 pairs
- Lumbar - 5 pairs
- Sacral - 5 pairs
- Coccygeal - 1 pair

![Spinal cord anatomy](spinal_cord_anatomy.jpg)

Spinal cord is made of gray and white matter (gray matter surrounded by white matter) and shows 4 surfaces namely posterior, anterior and 2 lateral.

Gray matter is comprised of neurons. White matter is made of axons. Pathways that connect brain with the rest of the body.

## ![Spinal nerves](spinal_nerve.jpg)

## References

- [Spinal cord](https://www.kenhub.com/en/library/anatomy/the-spinal-cord)
