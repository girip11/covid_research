# SARS-CoV-2 S1 Protein Persistence in SARS-CoV-2 Negative Post-Vaccination Individuals with Long COVID/ PASC-Like Symptoms

- PASC like symptoms 4 weeks after the vaccine.

## Study

- 50 post vax individuals with 10 healthy individuals and 35 individuals post vax without symptoms
- Cytokine and Chemokine profiling with spike protein detection in the monocyte subsets using flow cytometry and mass spectometry.

## Results

- Post vaccine symptoms similar to PASC patients.
- Statistically significant elevations of sCD40L, CCL5, IL-6 and IL-8. S1 and S2 protein were detected in the CD16+ monocytes.

## Discussion

- Average time to report symptoms - 105 days after vaccination.

> There was variability in symptoms according to the vaccine administered, however, the predominant symptoms were fatigue, neuropathy, brain fog and headache.

- Statistically significant elevation in CCL5, sCD40L, IL-8 and IL-6.

- Non classical and intermediate monocytes contain S1 protein. S1 +ve, CD16+ cells also contained peptide sequences of S2 and mutant s1 peptides

- CD16+ monocytes are highly mobile cells. The non classical monocytes bind to fractalkine expressed on vascular endothelial cells through the CX3CR1 receptor.

- Fractalkine upregulated by IL-1, IFN-gamma and TNF-alpha cytokines.
- Stress and exercise mobilize non classical moncytes.

- Fractalkine (on endothelial cells) and CX3CR1(monocytes) interaction is involved in the pathogenesis of atherosclerosis, vasculitis, inflammatory brain disorders.

- Vascular endothelitis - expose collagen and hence the subsequent platelet activation and clotting cascade. sCD40L marker for activated platelets.

- CCR5 antagonists(Maraviroc) reduce recruitment of non classical monocytes.
- Fractalkine expression on endothelial cells can be reduced with the treatment of statins

- VEGF - induces angiogenesis and microvascular hyperpermeability. VEGF contributes to vasculitic neuropathy.

## My takeaways

- Fractalkine is upregulated by INF-gamma and TNF-alpha which are acute phase cytokines.
- In the absence of such elevated cytokines, I am unable to comprehend why the fractalkine would be upregulated and hence increased binding with the monocytes.

- Statins/fenofibrate can cause increase in eNOS which could act as a platelet adhesion inhibitor.

---

## References

- [SARS-CoV-2 S1 Protein Persistence in SARS-CoV-2 Negative Post-Vaccination Individuals with Long COVID/ PASC-Like Symptoms](https://www.researchsquare.com/article/rs-1844677/v1)
