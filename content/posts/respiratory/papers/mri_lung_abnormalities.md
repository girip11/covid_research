# Hyperpolarized 129Xe MRI Abnormalities in Dyspneic Patients 3 Months after COVID-19 Pneumonia

## Preliminary results

Hyperpolarized xenon 129 MRI results showed alveolar capillary diffusion limitation in all nine patients after COVID-19 pneumonia, despite normal or nearly normal results at CT.

---

## References

- [Hyperpolarized 129Xe MRI Abnormalities in Dyspneic Patients 3 Months after COVID-19 Pneumonia: Preliminary Results](https://pubs.rsna.org/doi/full/10.1148/radiol.2021210033)
- [EXPLAIN Study](https://oxfordbrc.nihr.ac.uk/preprint_explain_study/)
