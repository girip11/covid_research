# Persistent Exertional Intolerance After COVID-19

## Summary

- Invasive Cardio Pulmonary Test(iCPET) performed on 10 healthy controls and 10 patients recovered from covid (age and sex matched controls).

- Patients recovered from covid exhibited reduced peak exercise aerobic capacity measured as $VO_{2}max$ as well as abnormal ventilatory inefficiency compared to the controls.

## My takeaways

- I couldnt find the information on the duration between patients recovering from covid and this testing(recently recovered or long covid which is 12 weeks post covid).

- Reason for the observations werent found yet. Authors suspect multiple reasons like mitochondiral dysfunction, oxgyen extraction issue etc.

---

## References

- [Persistent Exertional Intolerance After COVID-19: Insights From Invasive Cardiopulmonary Exercise Testing](https://pubmed.ncbi.nlm.nih.gov/34389297/)
