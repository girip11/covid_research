# Happy Hypoxia in covid

## Terminologies

- Vasodilation - is the widening of blood vessels. results from relaxation of smooth muscle cells within the vessel walls. Dilation increases the flow of the blood, decreases the BP, increases the cardiac output.
- Vasoconstriction - is the narrowing of blood vessels. reduces blood flow. important in controlling hemorrhage and increases the blood pressure. Exposing body to severe cold causes vasoconstriction.

## Highlights

> when the respiratory muscles are fatigued or weakened due to altered lung mechanics, breathing may be perceived as a substantial effort.
>
> The adequacy of gas exchange is primarily determined by the balance between pulmonary ventilation and capillary blood flow, referred as ventilation/perfusion (V/Q) matching.

## Causes of hypoxemia

### Intrapulmonary shunting

- Infection causes local interstitial edema(swelling due to fluid buildup)
- Increased lung edema leading to ground glass opacities
- Alveolar collapse ensues and blood perfuses the non-aerated lung tissue resulting in intrapulmonary shunting.
- Progressive edema as the infection progresses causing alveolar flooding.

### Loss of lung perfusion regulation

- Failure of pulmonary vasoconstriction mechanism can cause blood flow to non-aerated lung alveoli.
- Dysregulation of RAS(Renin-Angiotensis System) due to ACE2 receptor in the cells being hijacked by the virus. This causes AngII to increase in the blood because AngII is converted to Ang1-7 by ACE2 receptor. The virus also acts like ACE inhibitor increasing bradykinin(endothelial vasodilator that lowers BP) levels preventing its degradation. Bradykinin is released during inflammation on tissue(lung) damage. Increase in Bradykinin means vasoconstriction is affected. Bradykinin receptors belong to GPCR family.

### Intravascular microthrombi

- procoagulant activity - resulting from endothelial injury, complement activation.
- hypofibrinolytic activity - inhibition of plasminogen activation via Plasminogen Activator Inhibitor which is released during acute phase under IL-6 influence.
- Physiological dead space could increase due to reduced blood flow caused by intravascular thrombi.

### Impaired diffusion capacity

- Lung diffusion capacity (DLCO lung function test) can be impaired
- Lung diffusion - ability of pass oxygen from the alveolar air sacs into the blood.
- Virus replicates within alveolar type II cells, so these infected cells will be killed (apoptosis) leading to loss of alveolar epithelial cells.
- Loss of these cells means denuded basement membrane is left with debris of fibrin, dead cells etc.

## Therapeutic target

- Avoiding microthrombi and fibrin deposition.

> It seems prudent to use thromboprophylaxis in all COVID-19 patients, particularly in those with high D-dimers on admission.
>
> Effective hypoxic pulmonary vasoconstriction may be another target to improve the matching of regional perfusion and ventilation in the lung
>
> RAS modulation (e.g. angiotensin receptor blockers, recombinant soluble ACE2, and inhibition of the bradykinin system) may have a potential role in restoring lung perfusion regulation

---

## References

- [The pathophysiology of ‘happy’ hypoxemia in COVID-19](https://respiratory-research.biomedcentral.com/articles/10.1186/s12931-020-01462-5)
