# Lung abnormalities discovered in long COVID patients with breathlessness

## About the study

- This study is named `EXPLAIN`
- Patients presenting breathlessness even those who were never hospitalized.
- CT scan and lung function tests are normal yet patients suffer from breathlessness.
- Study long covid patients with breathlessness using hyperpolarized Xenon MRI for lung abnormalities.
- Inhaled hyperpolarized(for MRI detection) Xenon should move from lungs in to the bloodstream. Xenon behaves very similar to oxygen.

## Initial study

- Nine covid hospitalized patients and 5 healthy controls were enrolled.
- Abnormalities preventing oxygen in getting into the bloodstream was found.

## EXPLAIN study

### Patient groups

- Long covid patients with normal CT scans.
- Hospitalized patients 3 months post discharge with normal/near normal CT and don't experience long covid.
- age and gender matched control group with no prior covid hospitalization nor long covid.

### Findings

- Significantly impaired gas transfer

### What's next

The full EXPLAIN study will recruit 200 long COVID patients with breathlessness, along with 50 patients who have had COVID-19 but now have no symptoms at all; 50 patients who have no breathlessness, but do have other long COVID symptoms, such as ‘brain fog’; and 50 people who have never had long COVID who will act as controls for comparison.

## My takeaways

- 129Xenon MRI is required.

In the absence of this imaging technique, can venous oxygen saturation be used to detect impaired gas transfer.

Also how about the lung function test Diffusing Capacity of the Lungs for CarbonMonoxide(DLCO). DLCO is a measurement to assess the lungs' ability to transfer gas from inspired air to the bloodstream

---

## References

- [EXPLAIN Study](https://oxfordbrc.nihr.ac.uk/preprint_explain_study/)
- [Study confirms longer-term lung damage after COVID-19](https://oxfordbrc.nihr.ac.uk/study-confirms-longer-term-lung-damage-after-covid-19/)
- [Lung abnormalities found in long COVID patients with breathlessness](https://www.nihr.ac.uk/news/lung-abnormalities-found-in-long-covid-patients-with-breathlessness/29798)
