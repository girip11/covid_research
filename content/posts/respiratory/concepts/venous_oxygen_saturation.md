# Venous Oxygen Saturation (SvO2)

- measure of oxygen content of the blood returning to the right side of the heart after perfusing the entire body.

## Measurement

- Pulmonary artery catheter(PAC) measures `SmvO2`. Risky
- Central Venous Catheter (CVC) measures `ScvO2`. Less complications and cost effective. The catheter placement is confirmed by one of the following methods: chest radiography, fluoroscopy, ultrasound, or transesophageal echocardiography. A venous blood sample is drawn for ScvO2 measurement.

---

## References

- [Venous Oxygen Saturation](https://www.ncbi.nlm.nih.gov/books/NBK564395/)
