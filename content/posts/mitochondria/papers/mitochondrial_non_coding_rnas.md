# COVID-19 and Mitochondrial Non-Coding RNAs: New Insights From Published Data

## Terminologies

- Mitochondria contains DNA called mtDNA.
- PBMC - Peripheral blood mononuclear cells (PBMCs) are blood cells with round nuclei, such as monocytes, lymphocytes, and macrophages.

## Summary

- Hypothesis - dysfunction in the mitochondrial metabolism.
- This study explores the effect of Cov2 on the transcription of the mitochondrial non-coding RNAs.

> No change in the expression of long non-coding RNAs was detected at any stage of the infection, but up to 43 small mitochondrial RNAs have their expression altered during the recovery from COVID-19.
> This result suggests that the SARS-CoV-2 infection somehow affected the metabolism of small mitochondrial RNAs specifically without altering the overall mitochondrial transcription.

## Highlights

- Few studies identified disruption of the mitochondrial metabolic pathway but how the mechanism of disruption is not yet known.

- It has been shown that the gene expressions in mitochondrial genome(mtDNA) corresponding to ATP production is not impaired during covid-19.

- Canonical genes- well known 37 genes in the mtDNA(13 proteins, 22 tRNAs and 2rRNAs). Recently discovered genes are termed as non-canonical genes.

- Despite not being well characterized, it is known that at least one group of the non-canonical mitochondrial genes is involved in the immune response again viruses (double-stranded RNAs) thus making it plausible for other types of mitochondrial non-coding RNAs to be involved as well.

- This study focuses only on long and small RNAs (mt-lncRNAs and mt-sRNAs).

- By using the datasets available, it can be tested if mt-lncRNAs and mt-sRNAs expression changes at any, or all, stages of COVID-19 recovery, **which would suggest that these genes are either involved in the response to SARS-CoV-2 infection or disrupted by its presence (or both)**, thus suggesting a possible link to long COVID-19 syndrome.

## My takeaways

- How can the presence of viral proteins in the cell disrupt the mitochondrial metabolism?
- Viral remnants inside the cell possible in longcovid? How? Virus invaded cells should die/get killed.

---

## References

- [COVID-19 and Mitochondrial Non-Coding RNAs: New Insights From Published Data](https://www.frontiersin.org/articles/10.3389/fphys.2021.805005/full)
- [Gene expression](https://www.yourgenome.org/facts/what-is-gene-expression)
- [DNA](https://www.abc.net.au/science/slab/genome2001/dna.htm)
- [Gene, DNA, chromosome](https://www.youtube.com/watch?v=mcEV3m9SG9M)
