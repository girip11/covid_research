# Mitochondria

- Human cells contain two genomes and two protein synthesizing (translation) systems. The first is the **nuclear genome** of 3 × 10^9 base pairs that has 30,000 to 40,000 genes coding a much greater number of proteins whose mRNAs are translated by cytoplasmic ribosomes. This is what we saw above.
- The second genome is that found in a cytoplasmic organelle, the mitochondrion (mt). It contains a circular DNA of 16,850 base pairs (mtDNA) that codes for 2 rRNAs, 22 tRNAs, and 13 mRNAs

- We have two ribosomes as well. Cytoplasmic ribosome is present inside the cell alongside mitochondria and they are the protein factories.
- Nuclear genome encodes the cytoplasmic ribosome rRNAs and the ribosomal proteins for both cytoplasmic and mitochondria ribosomes while mtDNA encodes the 2 rRNAs.
- Mitochondria too has its own ribosome which is formed by importing the ribosomal proteins synthesized by the cytoplasmic ribosomes following the ribosome gene expressions in the nuclear genome.
- These imported proteins assemble with 2 rRNAs to form the mitochondria ribosome. This mitochondria ribosome is responsible for manufacturing the 13 proteins encoded by the mtDNA responsible for oxidative phosphorylation.
- Oxidative phosphorylation is the process of producing ATP from oxygen and simple sugars.
- 22 genes provide instructions for making molecules called transfer RNA (tRNA) and 2 ribosomal RNA (rRNA), which are chemical cousins of DNA. These types of RNA help assemble protein building blocks (amino acids) into functioning proteins.

## Non canonical genes

- These are newly discovered genes of the mitochondrial genome.
- Currently, there are at least 8 lncRNAs(Long Non-Coding), some dsRNAs, various small RNAs, and hundreds of circRNAs known to be generated from mitochondrial genome.

---

## References

- [Mitochondrial ribosomal proteins: Candidate genes for mitochondrial disease](https://www.nature.com/articles/gim200414)
- [Mitochondria Encoded Non-coding RNAs in Cell Physiology](https://www.frontiersin.org/articles/10.3389/fcell.2021.713729/full)
