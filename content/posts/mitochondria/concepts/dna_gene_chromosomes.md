# DNA, Gene and Chromosomes

- Each cell consists of the entire genetic material called genome and the genome is made up of DNA.

## DNA

- DNA is made of four bases(nucleotides). They are Adenine, Thymine, Cytosine and Guanine.
- Double stranded like a zip. Each strand is formed from the four bases. A and T form pairs while C and G form pairs(base pairs).
- The order of these bases determine our unique genetic code.
- DNA contains the instructions for producing proteins that our bodies need.
- We only need one strand to figure out the other.
- Sample portion of the DNA

```text
    A -> T
    T -> A
    T -> A
    C -> G
    G -> C
    A -> T
    C -> G
```

- Each DNA consits of around 3 billion nucleotides (long sequence)
- During cell multiplication, the cell uses enzymes to make copies of DNA.
- The cell also uses another set of enzymes to verify the constructed DNA and repair if any damage.

## Gene

- Sections of DNA
- Within DNA, a gene may be made up of a sequence consisting of thousands of the four nucleotides.
- DNA is always read 3 letters(bases) at a time called codon which correspond to an amino acid.
- Human genome consists of approximately 20,687 protein encoding genes.
- Each cell consists of two set of genes, one from mother and the other from father.
- The genes are packed into 46 chromosomes for each of storage and access.

## Chromosomes

- Bundles of tightly coiled DNA located within the nucles of almost every cell in the body.
- 46 chromosomes with 23 from mother and 23 from father leading to 23 pairs (or 46).

## Gene expression

- Gene expression - the process by which instructions present in the DNA are used to produce the final functional protein. We know the DNA in each gene contains information on how to make a protein.

- Gene expression is responsible for enabling/disabling of a protein synthesis as well as controlling the volume of the protein to be made.

- There are two key steps in making a protein. Transcription and translation.

### Transcription

- Gene copied to RNA transcript to produce messenger RNA using RNA polymerase enzyme. We know DNA consists of 4 bases (Adenine-Thymine pairs, Guanine-Cytosine pair). In RNA, Uracil replaces the base Thymine.

### Translation

- Transcribed message carried by mRNA to the ribosomes and transfer RNA reads the message. mRNA is read the letters (possible letters are using four RNA bases A U G C) at a time called codon. Each codon specified a particular amino acid. In humans we only have 20 amino acids. 64 combinations of codons map to these 20 amino acids (many codons can map to the same amino acid).
- 1 tRNA molecule binds to a single codon (amino acid) in the mRNA
- Each of these bound tRNA molecule delivers the attached amino acid(aka codon) to the ribosome and all the amino acids join using the peptide bond to form polypeptides.

## Upregulation and downregulation

Think downregulation and upregulation with respect to homeostatis.

- Downregulation is the process by which a cell decreases the quantity of a cellular component such as RNA/protein in response to external stimulus.
- Example decrease in the expression of a specific receptor in response to its increased activation by a molecule, such as a hormone or neurotransmitter, which reduces the cell's sensitivity to the molecule.(Increased molecule means increased activation. To achieve homeostatis we need to downregulate the receptor to those molecules)
- Upregulation is complementary to downregulation.
- Example response of liver cells exposed to such xenobiotic molecules as dioxin. In this situation, the cells increase their production of cytochrome P450 enzymes, which in turn increases degradation of these dioxin molecules(to keep the dioxin levels in check cells need to upregulate production of enzymes that degrade it).

- Considering together the results from canonical and non-canonical genes it can be concluded that SARS-CoV-2 infection does not affect general mitochondrial transcription but, **rather, affects the biogenesis pathways of specific mitochondrial products. This effect seems to be more marked on the mt-sRNAs rather than longer ones, although the reason for such a pattern can only be speculated at this stage.**

- Disruption is limited only to the mt-sRNAs.

---

## References

- [Gene expression](https://www.yourgenome.org/facts/what-is-gene-expression)
- [DNA](https://www.abc.net.au/science/slab/genome2001/dna.htm)
- [Gene, DNA, chromosome](https://www.youtube.com/watch?v=mcEV3m9SG9M)
- [Downregulation and upregulation](https://en.wikipedia.org/wiki/Downregulation_and_upregulation)
