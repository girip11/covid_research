# SARS CoV‐2 related microvascular damage and symptoms during and after COVID‐19: Consequences of capillary transit‐time changes, tissue hypoxia and inflammation

## Terminologies

- Hypoxemia - low oxygen levels in the blood.

## Hypothesis

> how capillary damage and inflammation may contribute to these acute and persisting COVID‐19 symptoms by interfering with blood and tissue oxygenation and with brain function.
>
> Capillary flow disturbances limit oxygen diffusion exchange in lungs and tissue and may therefore cause hypoxemia and tissue hypoxia
>
> It identifies a vicious cycle, as infection‐ and hypoxia‐related inflammation cause capillary function to deteriorate, which in turn accelerates hypoxia‐related inflammation and tissue damage.

## Highlights

> Blood's capillary transit time, determines the time availability for blood‐tissue oxygen exchange, and any deviations from a homogenous distribution of blood across the capillary bed therefore reduce the capillary bed's functional capillary permeability surface area product (PS) for oxygen transport
> Therefore, blood's uptake of oxygen in the lungs, and the uptake of oxygen by tissue, not only depend on blood flow, but also on bloods microscopic distribution across the capillary bed.
> Reductions in tissue oxygen levels activate inflammation and cytokine release , which may interfere with neurotransmission, just as oxygen is important for the brain's serotonin synthesis(affecting the mood)

### Capillary changes and oxygenation

- Oxygen uptake in lung alveoli limited by capillary blood's mean transit time (MTT), (i.e) the time available for blood‐air oxygen exchange in lung alveoli before blood returns the heart.

- Capillary occlusion(blockage due to microthrombi) can affect oxygen intake.
- Angiogenesis is triggered which could form chaotic microvessels.

> In ARDS, fluid, immune cells, and cell debris accumulate in lung alveoli and within alveolar walls, preventing gas exchange so poorly oxygenated, CO2‐rich blood returns to the heart – so‐called right‐to‐left shunting. Hypoxic vasoconstriction counteracts such shunting and matches perfusion and ventilation across normal lungs by limiting blood flow through poorly ventilated alveoli.

- Transit time effects limit blood's oxygen uptake even in well‐aerated alveoli.
- CO2 exchange is largely insensitive to transit‐time effects due to its high permeability.

> In patients with well‐ventilated alveoli, transit‐time effects are therefore expected to cause normocapnic hypoxemia, and may therefore contribute to “silent hypoxia” because hypercapnia, in some, confers the sense of breathlessness.

### Capillary dysfunction and oxygen availability

> Any capillary flow disturbances, for example, due to endothelial swelling or the slow passage of activated neutrophils through some capillaries, shorten bloods transit times through the remaining, patent capillaries, limiting oxygen uptake.
> Capillary transit‐time heterogeneity (CTH) quantifies the extent of capillary flow disturbances, typically in terms of the standard deviation of capillary transit times or blood velocities within a microscopic tissue volume.

- Reduced blood flow to tissues will cause angiogenesis which is new blood vessel formation, so that the oxygen supply to the tissues is restored.

- This new blood vessed formation may increase the capillary transit time heterogeneity.

### Effect of covid on the microcirculation

> The virus‐ACE2 receptor interaction is thought to reduce ACE2 action and increase levels of angiotensin II, a powerful capillary and arteriolar vasoconstrictor.
> With respect to COVID‐19 respiratory symptoms, alveolar epithelial, and capillary endothelial cells express ACE2 receptors and SARS‐CoV‐2 particles are found within both cell types in COVID‐19 patients

## My takeaways

- This vicious cycle `infection and acute phase hypoxia -> inflammation -> capillary damage -> hypoxia -> inflammation and tissue damage`

- Alveolar capillary microthrombosis and capillary shunting

- Can hypoxic vasoconstriction in lungs be addressed with vasodilators like NO(L-arginine, statins/fenofibrate that increase eNOS expression)

- Would it help to measure the serum Angiotensin II levels?

---

## References

- [SARS CoV‐2 related microvascular damage and symptoms during and after COVID‐19: Consequences of capillary transit‐time changes, tissue hypoxia and inflammation](https://pubmed.ncbi.nlm.nih.gov/33523608/)
