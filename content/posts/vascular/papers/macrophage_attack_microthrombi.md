# A macrophage attack culminating in microthromboses characterizes COVID 19 pneumonia

## Terminologies

- Macrophages are part of the immune system. Monocytes a type of WBC differentiate in to either macrophage or dendritic cells.

- Dendritic cells alert other cells to fight the antigen by releasing cytokines while macrophages fights and kills the antigen/cancerous and dead cells (phagocytosis).

- Necrosis - death of body tissues due to lack of blood supply.

## Hypothesis

Macrophages which are part of the innate immune system becomes overactive during acute covid and this is linked with the elevated fibrin in the blood.

## Summary

- Macrophages can destroy the Covid affected cells(paper discusses especially the virus laden alveoli cells in the lungs) as part of phagocytosis.
- Macrophages are rich in thromboplastin which is prothrombotic in nature.
- This immune overreaction confirmed by elevated ferritin, acute phase proteins, D-dimer suggesting clotting.
- All of fibrinogen converted to fibrin resulting in showers of microclots. These microclots can be harmful when the rate of fibrinolysis is slow compared to the rate of microclot formation, thereby causing **coagulation necrosis** during acute phase.

## Highlights

## My takeaways

- Note that here the microclots formed can be fibrinolysed. Elevated D-Dimer is indicative of clots that are being subjected to fibrinolysis. And this paper discusses the **microclots formed during acute phase** and since these are fibrin based these should be fibrinolysed soon.

- But what if endothelium is affected and hence [fibrinolysis is impaired](./../impaired_fibrinolysis.md) during acute phase. Then these acute phase microclots can persist and the body can break down these clots slowly as the endothelium heals and that could be the reason why some of these clots show up later in the scans of [brain](https://www.walesonline.co.uk/news/health/doctor-long-covid-found-multiple-22277494), [lungs](https://mylongcoviddiaries.medium.com/i-finally-have-a-diagnosis-for-long-covid-and-its-shocking-82ddcb214656) etc. Post anticoagulants many patients I know have their clots disappeared.

- Microclots that are amyloid based are resistant to fibrinolysis. This paper does not discuss such microclots.

## My Thoughts

- Rather than measuring only D-dimer if we can **measure fibrin, antiplasmin, plasminogen activator inhibitor 1, TEG test along with D-dimer**, that would give us more accurate picture of the coagulation, given we know the following
  - Fibrinolysis can be impaired in severe covid. But this is not well studied on long covid patients.
  - SARS-Cov2 uses ACE2 as entry to the cell and can cause endothelial dysfunction which inturn can disrupt the release of tPA.

> In health, the endothelium prevents thrombus formation through a number of mechanisms. **Thrombomodulin, protein S, heparan sulfate proteoglycans, and tissue factor pathway inhibitor are all endothelium-derived inhibitors of coagulation, whereas prostacyclin, nitric oxide (NO), and surface-bound CD39 inhibit platelet aggregation**. However, when endothelial function is perturbed, for example with injury or **inflammation**, it can rapidly become procoagulant by downregulating its anticoagulant functions, inducing tissue factor expression and increasing secretion of factors such as fibronectin, von Willebrand factor (vWF), and platelet activating factor - [Stimulated Tissue Plasminogen Activator Release as a Marker of Endothelial Function in Humans](https://www.ahajournals.org/doi/10.1161/01.ATV.0000189309.05924.88)

- If we can test patients for impaired fibrinolysis, we can treat with [tPA medications](https://www.ncbi.nlm.nih.gov/books/NBK507917/) which are often used in the treatment of ischemic stroke. Or using supplements like nattokinase which are known to have fibrinolytic properties.

---

## References

- [A macrophage attack culminating in microthromboses characterizes COVID 19 pneumonia](https://onlinelibrary.wiley.com/doi/full/10.1002/iid3.482)
