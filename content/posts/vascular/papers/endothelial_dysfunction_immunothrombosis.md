# Endothelial dysfunction and immunothrombosis as key pathogenic mechanisms in COVID-19

## Terminologies

- Immunothrombosis - the immune and coagulation systems cooperate to block pathogens and limit their spread

> - The intrinsic pathway consists of factors I, II, IX, X, XI, and XII. Respectively, each one is named, fibrinogen, prothrombin, Christmas factor, Stuart-Prower factor, plasma thromboplastin, and Hageman factor. The extrinsic pathway consists of factors I, II, VII, and X. Factor VII is called stable factor. The common pathway consists of factors I, II, V, VIII, X.
> - Extrinsic pathway of coagulation is activated through **Tissue Factor** released by endothelial cells after external damage.Intrinsic pathway of coagulation is activated through exposed endothelial collagen.
> - [Coagulation pathways][1]

- Endothelium responsible in regulating haemostasis, fibrinolysis and vessel wall permeability.

## Hypothesis

> SARS-CoV-2 infection induces a process known as **immunothrombosis**, in which activated neutrophils and monocytes interact with platelets and the coagulation cascade, leading to intravascular clot formation in small and larger vessels.

## Highlights

### Immunothrombosis

> severe-to-critical disease show a **dysregulated hyperactivation** of the immune system that can cause an abnormal cytokine immune response.

- Monocytes activate extrinsic pathway of coagulation through Tissue factor on their surface
- Monocytes also release proinflammatory cytokines that recruit neutrophils which inturn release Neutrophil Extracellular Traps(NET) that can recruit platelets via VonWillebrand Factor and activate them.
- NETs inactivate the natural anticoagulants too.
- NETs can also activate extrinsic coagulation pathway by binding to Tissue Factor.
- Platelets can be activated in multiple ways and those activated platelets too release proinflammatory cytokines.
- Sars-cov2 virus can be trapped in fibrin based NETs and killed.

> The immunothrombotic process allows pathogen killing to be restricted to the intravascular compartment, thus limiting injury to organs.

### Coagulopathy

> - The occurrence of a COVID-19-specific coagulopathy is suggested by elevated levels of fibrinogen, von Willebrand factor (VWF) and the fibrin degradation product D-dimer in the blood, whereas patients generally show minor or no changes in prothrombin time (a measure of time to clot), activated partial thromboplastin time (coagulation time), antithrombin levels, activated protein C levels and platelet count.
> - SARS-CoV-2 directly infects vascular endothelial cells and leads to cellular damage and apoptosis, thus decreasing the antithrombotic activity of the normal endothelium
> - Endothelial cells of lung blood vessels can be activated by the high levels of pro-inflammatory cytokines (IL-1, IL-6 and TNF) and ferritin in severe COVID-19
> - Increased levels of the endothelial adhesion protein VWF were reported both in patients admitted to the ICU and in non-critically ill patients.

### Endothelial dysfunction

> - Higher levels of soluble P-selectin (a marker of endothelial and platelet activation) were observed in patients admitted to the ICU than in patients not in the ICU, whereas increased levels of thrombomodulin (a specific marker of endothelial activation generally released during endothelial cell injury) were associated with increased mortality risk
> - Higher numbers of circulating endothelial cells were described in patients with COVID-19, especially among those admitted to the ICU

#### Coagulation Inflammation Cycle

> - Endothelial dysfunction is recognized as a risk factor of microvascular dysfunction through a shift towards vasoconstriction, by promoting ischaemia, inflammation and a procoagulant state
> - The inflammatory environment triggers the expression of activated tissue factor on endothelial cells, macrophages and neutrophils, which amplifies activation of the coagulation cascade

#### Hyperactivated platelets

> - Platelets were found to be hyperactivated in patients with COVID-19
> - Activated platelets are critically involved in neutrophil extracellular trap (NET) formation, which is an essential element of immunothrombosis.

### ARDS

> Endothelial damage breaking down pulmonary vasoregulation, stimulating **ventilation–perfusion mismatch** — which is mainly responsible for hypoxaemia and supporting immunothrombosis.

Can this be the reason why patients have desaturation, abnormal blood flow, clots in the lungs, brain etc? Microvascular endothelium damage could be causing the breathlessness, cognitive dysfunction.

### NETs

> - NETs participate in acute lung injury by inducing macrophage release of IL-1β, which in turn can increase NET formation.
> - The observation of antiphospholipid antibodies, which can directly stimulate NETosis, in patients with COVID-19, as well as in other infections93, suggests that SARS-CoV-2 infection might synergize with antiphospholipid antibodies to promote the immunothrombotic process.

## Proposed Therapies

- Anticoagulant. Ex- Direct oral anticoags, or inhaled heparin

- A Factor XII-blocking antibody (garadacimab) is currently being explored in a multicentre, double-blind, placebo-controlled phase II trial in patients with respiratory distress and COVID-19

- Tissue plasminogen activator usually used to treat acute ischemic stroke is being trialled.

- Antiplatelet agent dipyridamole. Trial ongoing

- NET inhibition can be anti-inflammatory.

- Two FDA-approved complement inhibitors are available, eculizumab and ravulizumab

## My thoughts

**Endotheliopathy** - could be one of the primary long covid culprits.

- This paper too arrives at platelet activation and microthrombosis causing ARDS. Once again its discusses on vascular side of things. So as long as Covid is not treated like a immunothromobotic condition, adverse outcomes are unavoidable.

- I always thought endothelial damage activates the platelets. Here the paper seems to touch upon neutrophils and monocytes interaction with platelets.

- In my opinion you don't need to have a severe covid(ICU) to have microthrombosis. Even a mild case(symptomatic say fever) can still lead to microclots (My assumption is these clots can be fibrinolysed but due to endothelial dysfunction these clots stay in the body for long).

- One could see long covid as endothelial dysfunction disease, the residual microclots and ongoing clot formation(if any) could be gotten rid with anticoagulant and activation of platelets can be done with antiplatelet therapy.

- MECFS patients too are known to exhibit endothelial dysfunction. PEM has been known to occur in people with endothelial dysfunction.

- With ongoing endothelium damage, when LC, MECFS folks get vaccination, spike is going to injure the endothelium further, resulting in clotting cascade. This should be the reason why we have microclots in our blood of vax injured.

- People who have recovered seem to have their endothelium healed completely.

- NET fibrin clots trap the virus as per this paper.

How can we correct the endothelial dysfunction?

---

## References

- [Endothelial dysfunction and immunothrombosis as key pathogenic mechanisms in COVID-19](https://www.nature.com/articles/s41577-021-00536-9)

- [Coagulation Pathways][1]

[1]: https://www.ncbi.nlm.nih.gov/books/NBK482253/
