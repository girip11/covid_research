# Serine proteases in Virus proteolytic activation

The viral spike S glycoprotein binds to the human receptor angiotensin-converting enzyme 2 (ACE2) and then is activated by the host proteases.

## Terminologies

- Protease enzyme - enzyme that catalyzes proteolysis.
- Proteolysis - breakdown of proteins/peptides into smaller polypeptides/amino acisd by the action of enzymes. Example S protein cleaved to S1 and S2 proteins.
- Remember proteins are nothing but folded amino acid chains. In human body total of 20 amino acids are found. In other words all proteins made in our body come up those 20 amino acids.
- Peptide - Two amino acids joined together by peptide bonds.
- Polypeptide chain - Hundred or more amino acids linked with covalent peptide bonds. Many Polypeptide chains combine to form a protein. In otherwords, proteins are long chains of amino acids connected by peptide bonds.
- Glycoprotein - any of a class of proteins which have carbohydrate groups attached to the polypeptide chain.
- Hyaline membranes - These membranes are made up of **dead cells, surfactant, and proteins**. The hyaline membranes deposit along the walls of the alveoli, where gas exchange typically occurs, thereby making gas exchange difficult.

## Role of Serine Proteases and Host Cell Receptors Involved in Proteolytic Activation, Entry of SARS-CoV-2 and Its Current Therapeutic Options

### Summary

- Virus entry into the host cell through ACE2 receptor and glucose regulated protein 78(GRP78)
- Proteolytic activation of Spike protein requires host cell serine proteases including TMPRSS2, cathepsins, trypsin and furin.
- S protein has S1 and S2 subunits. S1 has RBD that can bind to ACE2 receptor on the host cell surface.
- Fusion of SARS-CoV-2 requires proteolytic cleavage of spike glycoprotein at the boundary of S1 and S2 subunits.
- Proteolytic Activation of spike protein requires the activity of **TMPRSS2** to cleave it.
- The fusion of viral-host membrane requires **furin**-based priming at the S1/S2 site of S protein
- In addition to TMPRSS2 and furin, cathepsins induce endocytic-based fusion of host cell and viral membrane

### Highlights

![ACE2_TMPRSS2_Entry](ace2_tmprss2.jpg)

- Spike (S) protein is a glycoprotein (cytokines are also glycoproteins).

- After the virus binds to ACE2 of the host cell, TMPRSS2 activates priming and fusion of virus-host cell membrane, which also leads to the entry of the virus to the host cell.

- Cathepsin B/L undergoes their proteolytic activation after endocytosis(after virus entry in to the cell) of the virus. The cathepsin found in the lysosome facilitates endocytic dependent entry of SARS-CoV-2 to the host cell

- Furin can also be involved in cleavage of S spike glycoprotein.

- Trypsin can also cleave the S spike protein at S1/S2

## Host Serine Proteases: A Potential Targeted Therapy for COVID-19 and Influenza

![Sars_cov2_structure](sars_cov2_virus_structure.jpg)

- Endosomal activation - cathepsin protease cleaving inside endosomes post endocytosis.
- Non endosomal pathway - cleavage by Furin protease enzyme followed by TMPRSS2

> Although binding to host cells is the initial step of infection, virus entrance necessities the cleavage of S protein via host proteases including cell surface transmembrane protease/serine (TMPRSS) proteases, cathepsins, furin, elastase, factor Xa, and trypsin.

### Furin

> Furin plays a key role in the cleavage of a wide range of critical cell surface proteins including adhesion molecules, surface receptors, growth factors, and hormones to produce mature proteins.
>
> Additionally, furin cleaves envelope glycoproteins of different viruses, thus, improving the fusion of viral membrane with the host cell. Data indicate that the existence of a redundant furin cleavage site at S protein of SARS-CoV-2 is responsible for its infectious nature than other CoVs, leading to its higher efficiency to fuse to the host membrane

### Elastase

> Neutrophils as a part of the host defense system, release a granular serine protease (the so-called elastase) in response to a viral infection
> Increased elastase activity mediates acute lung injury through increasing inflammatory reactions (e.g., increasing vascular permeability, induction of pro-inflammatory cytokines such as IL-8 and IL-6 secretion by neutrophil vesicles, and conversion of pro-IL-1β to IL-1β)

### Plasmin

Plasmin breaksdown the fibrin(fibrinolysis) as well as helps virus to replicate through proteolysis[4].

> viruses, convert plasminogen to plasmin in order to cleave surface proteins and subsequently can evade the immune system or infect host cells
> plasmin can provoke cytokine production and stimulate inflammation via factor XII/bradykinin which subsequently can increase edema
> Plasmin can cleave hyaline membranes (consist of a fibrin network combined with serum proteins and cellular debris, acts as barriers to gas exchange in the alveoli)

[4] says early in the infection(mild/moderate), its better to inhibit fibrinolysis(DAPT and DOAG)

> the PlgRKT plasminogen receptor is differentially expressed on monocytes depending on their state of inflammation and can regulate phenotypic and functional changes in macrophages.

## COVID-19: Targeting Proteases in Viral Invasion and Host Immune Response

> In one of the reports from Wuhan, increased levels of serum IL-6, neutrophils, and C-reactive protein with reduced total lymphocytes were observed in a study on 99 patients
>
> A recent study has revealed the association between human serine proteases trypsin, thrombin, tryptase, and elastase with increased expression of MCP-1. nhibition of these proteases resulted in the inhibition of MCP-1 secretion via inactivation of various protease-activated receptors (PARs), leading to the abolishment of its chemotactic activity.

## The Potential Role of Coagulation Factor Xa in the Pathophysiology of COVID-19: A Role for Anticoagulants as Multimodal Therapeutic Agents

> FXa, a serine protease, has been shown to play a role in the cleavage of SARS-CoV-1 spike protein (SP), with the inhibition of FXa resulting in the inhibition of viral infectivity.
> we postulate as to the potential therapeutic role of FXa inhibitors as a prophylactic and therapeutic treatment for high-risk patients with COVID-19.
>
> TMPRSS2 colocalizes with ACE2 on multiple cell types, including type II pneumocytes and cardiac myocytes and is thought to play a key role in the infection of the pulmonary and cardiovascular system
> Similarly, additional studies have demonstrated the role of various coagulation factors, including FXa, factor IIa (FIIa; thrombin), and plasmin, as proteases, which act upon SARS SP

---

## References

1. [Role of Serine Proteases and Host Cell Receptors Involved in Proteolytic Activation, Entry of SARS-CoV-2 and Its Current Therapeutic Options](https://www.dovepress.com/role-of-serine-proteases-and-host-cell-receptors-involved-in-proteolyt-peer-reviewed-fulltext-article-IDR)
2. [Host Serine Proteases: A Potential Targeted Therapy for COVID-19 and Influenza](https://www.frontiersin.org/articles/10.3389/fmolb.2021.725528/full)
3. [COVID-19: Targeting Proteases in Viral Invasion and Host Immune Response](https://www.frontiersin.org/articles/10.3389/fmolb.2020.00215/full)
4. [PLASMIN: FRIEND OR FOE?](https://onlinelibrary.wiley.com/doi/10.1111/jth.14960)
5. [The Potential Role of Coagulation Factor Xa in the Pathophysiology of COVID-19: A Role for Anticoagulants as Multimodal Therapeutic Agents](https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC7541169/)
