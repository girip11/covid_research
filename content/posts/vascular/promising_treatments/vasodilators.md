# Vasodilators

Here I just explore the idea of using vasodilators in long covid.

## Supplements

### EicosaPentaenoic Acid(EPA)

> Eicosapentaenoic acid (EPA) is an omega-3 fatty acid that is found along with docosahexaenoic acid (DHA) in cold-water fish, including tuna and salmon. EPA prevents the blood from clotting easily, reduces triglyceride levels in the blood, and has effects that might reduce pain and swelling.

Cod liver oil contains high levels of EPA and DHA as well as vitamins A and D.
EPA is precursor to prostacyclin a vasodilator and platelet aggregation inhibitor.

### L-arginine

- important in producing NO by the endothelial cells.
- NO helps in vasodilation as well as inhibits platelet adhesion to the endothelium.

## CoQ10

- Coenzyme Q10 supplementation is associated with significant improvement in [endothelial function][4].

- It would appear that in a young population CoQ10 has no effect on the nitric oxide vasodilator pathway in skin but [does influence other vasodilator pathways][5].

### Acetyl l-carnitine and Alpha lipoic acid

> Mitochondria produce reactive oxygen species that may contribute to vascular dysfunction. alpha-Lipoic acid and acetyl-L-carnitine reduce oxidative stress and improve mitochondrial function.
>
> Active treatment consisted of tablets containing 200 mg of α‐lipoic acid and 500 mg of acetyl‐L‐carnitine and the placebo consisted of a similar‐appearing tablet containing vehicle alone. Subjects were instructed to take 1 tablet twice daily.

## Medications

These medications require physician's consultation. I am just noting down these medications on the basis of some papers mentioning vasodilation effect of these drugs.

### Statin/Fenofibrate

- Mainly taken for dyslipidemia
- Increase eNOS expression which enhances NO production

### Alpha Adrenergic Receptor Blocking Agent

α-Blockers block neuromuscular transmission by occupying the postsynaptic α1-adrenoceptor on the smooth muscle cell, causing vasodilation. Drugs are phenoxybenzamine, prazosin, doxazosin, terazosin, phentolamine.

### Eliquis

- Patients **detected** with microclots may be prescribed this blood thinner.
- [Eliquis also has vasodilation properties][2].

### Third generation betablockers

> Nebivolol and carvedilol are third-generation β-adrenoreceptor antagonists, which unlike classic β-blockers, have additional endothelium-dependent vasodilating properties specifically related to microcirculation by a molecular mechanism that still remains unclear. We hypothesized that nebivolol and carvedilol stimulate NO release from microvascular endothelial cells by extracellular ATP, which is a well-established potent autocrine and paracrine signaling factor modulating a variety of cellular functions through the activation of P2-purinoceptors.

---

## References

[1]: https://pubmed.ncbi.nlm.nih.gov/17396066/ "Effect of combined treatment with alpha-Lipoic acid and acetyl-L-carnitine on vascular function and blood pressure in patients with coronary artery disease"
[2]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5513931/
[3]: https://www.ahajournals.org/doi/10.1161/01.cir.0000066912.58385.de
[4]: https://pubmed.ncbi.nlm.nih.gov/22088605/
[5]: https://www.jptrs.org/journal/view.html?volume=1&number=1&spage=6
