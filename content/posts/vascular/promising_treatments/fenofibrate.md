# Fenofibrate

## Endothelial-dependent Dilation(EDD)

- Fenofibrate is pleiotropic. It can improve the endothelium dependent vasodilation and reduce the oxidative stress in normolipidemic adults

> Age-related impairments in EDD are a result of increased oxidative stress and decreased bioavailability of the vascular protective vasodilator, nitric oxide (NO)
> In a double-blind parallel group design, subjects were assigned to receive 7 days of fenofibrate (145mg/day) or placebo by block randomization.

### Results

> In vascular endothelial cells sampled from peripheral veins of the subjects, endothelial nitric oxide synthase(eNOS) protein expression was unchanged with placebo and after 2 days of fenofibrate, **but was increased after 7 days of fenofibrate**
> With regard to arterial blood pressure, the effect of fenofibrate is inconsistent, with reports of no effect or decreases in diastolic blood pressure.

## Study for use in Covid

- A research team led by the University of Birmingham and Keele University in the UK has found that a licensed oral drug, fenofibrate, and its active form, fenofibric acid, can substantially reduce SARS-CoV-2 infection in human cells in the laboratory.

- The detailed evidence for a positive effect from laboratory-based studies with fenofibrate suggests that randomised controlled trials are required to properly evaluate whether fenofibrate, as well as statin treatment, with low acquisition costs, have COVID-19 disease-modifying potential in hospitalised patients and in routine clinical practice.

## [Fenofibrate in Covid-19][1]

> Fenofibrate has anti-inflammatory, anti-oxidant and anti-angiogenic effects as well. They help to reduce tumor necrosis factor alpha (TNF-alpha), oxidative stress, Interleukin-6 (IL-6), vascular endothelial growth factor-1 (VEGF) signaling and are also effective in lowering fibrinogen levels

---

## References

- [Fenofibrate improves vascular endothelial function by reducing oxidative stress while increasing endothelial nitric oxide synthase in healthy normolipidemic older adults](https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC3684180/)
- [Study finds fenofibrate’s ability to reduce Covid-19 infection](https://www.clinicaltrialsarena.com/news/study-fenofibrate-covid-infection/)
- [The role of fenofibrate in the treatment of COVID-19][1]

[1]: https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC8563083/
