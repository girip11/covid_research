# Endothelial Dysfunction Driven by Hypoxia

## Terminologies

- Glycolysis - metabolic pathway that converts glucose to pyruvic acid. The free-energy released is used to form ATP(adenosine triphosphate). This pathway doesnot require oxygen.

- Lactate dehydrogenase converts pyruvate which is the final product of glycolysis to lactate when oxygen is absent/short in supply.

## Hypothesis

- Factors that interfere with the synthesis/metabolism of NO in endothelial cells are involved in Cardiovascular disease pathogenesis.

- Diminished tissue oxygen reported to influence endothelial NO bioavailability.

> In endothelial cells, NO is produced by endothelial nitric oxide synthase (eNOS) from **L-Arg**, with tetrahydrobiopterin (BH4) as an essential cofactor.
> eNOS uncoupling and the resulting oxidative stress is the major driver of endothelial dysfunction and atherogenesis.

## Highlights

### Hypoxia and endothelium

> Endothelium-dependent regulation of vascular tone is dependent on several vasoactive factors. The most important vasodilators include nitric oxide (NO), prostacyclin (PGI2), and endothelium-derived hyperpolarizing factor (EDHF), whereas vasoconstrictive factors are mainly endothelin-1 (ET-1), thromboxane (TXA2), and angiotensin II.
>
> NO dilates blood vessels, inhibits vascular smooth muscle cell proliferation, platelet aggregation, and leukocyte adhesion; thus, adequate NO bioavailability determines vascular health.
>
> Low blood oxygen (hypoxemia) may result from reduced inspired oxygen tension (e.g., high altitude sickness), respiratory system pathologies, e.g., asthma, chronic obstructive pulmonary disease (COPD), pneumonia, acute respiratory distress syndrome (ARDS)—also in the course of COVID-19, or other pathologies (e.g., anemia, sleep apnea, heart failure)
>
> local hypoxia caused by impaired oxygen distribution from blood to tissues accompanies many pathological states, such as stroke, myocardial infarction, atherosclerosis, and cancer, and contributes to the exacerbation of adverse lesions
>
> The HIF-driven transcriptional response allows adaptation to low oxygen levels or counteracts the effects of hypoxia, i.a. by inducing the expression of genes promoting **erythropoiesis, angiogenesis, and glycolysis**

- HIF-1 associated with induction of genes associated with the glycolytic pathway
- HIF-2 drives the induction of angiogenesis by stimulating erythropoietin expression.
- Both HIF-1 and HIF-2 share **VEGF-A**(angiogenesis) and glucose transporter-1(glycolysis)

### eNOS regulation

- NO produced by L-arg and O2 catalyzed by nitric oxide synthase (NOS)
- NOS three isoforms - neuronal(nNOS), inducible(iNOS) by proinflammatory cytokines, endothelial(eNOS)

> Different NOS isoforms generate NO at different rates, and NO concentration is a key determinant of its function. iNOS is the most potent NO donor, and high NO concentrations (e.g., produced by activated macrophages) have a cytostatic and cytotoxic effect. eNOS, in turn, generates the lowest NO levels capable of activating soluble guanylate cyclase (sGC) to generate the second messenger cGMP resulting in i.a. vasorelaxation and inhibition of platelet aggregation, thus preventing atherogenesis. (i.e) vascular homeostatis

- Cofactor `BH4`(tetrahydrobiopterin) is essential for optimal eNOS activity.
- eNOS activity is strictly related to intracellular calcium concentration.**Increase in intracellular calcium** triggers the stimulation of NO synthesis.

> Activation of eNOS may occur in response to diverse stimuli, including shear stress(blood flow-induced), acetylcholine, bradykinin, or hormones, acting through changes in eNOS interactions or its phosphorylation status
>
> eNOS activity can also be stimulated by receptor-dependent agonists (acetylcholine, bradykinin) acting through specific receptors that activate G-protein-dependent signaling pathways, leading to the release of intracellular calcium, and eNOS activation
>
> Endothelial NO production is also influenced by hormones that rapidly affect eNOS activity by altering its phosphorylation or modulate the amount of eNOS protein. Insulin, thyroid hormones, and estrogen have been shown to increase eNOS expression. Insulin resistance and hypothyroidism are associated with reduced eNOS activity and increased risk of CVD
>
> Estrogen is thought to significantly inhibit the development of atherosclerosis through stimulation of eNOS expression and its activity. High estrogen levels in women reduce the risk of cardiovascular disease, but the risk dramatically increases after menopause

### eNOS uncoupling

- to produce NO, electron transfer from NADPG to L-arg needs to happen. In case of eNOS uncoupling, the electrons leak from the transport chain and transferred to yield superoxide. This is harmful and reduces NO bioavailability.

### Hypoxia and CVD

> Insufficient blood oxygenation, in turn, evokes hypoxic pulmonary vasoconstriction, which, if it persists, eventually leads to the development of pulmonary hypertension (PH).
>
> Interestingly, the pathophysiology of PH has been linked to the disruption of the NO pathway. Intrapulmonary NO levels were reported to be decreased in PH patients, implicating that endothelial dysfunction is involved in the pathogenesis of PH.
> Hypoxia was also detected in human atherosclerotic carotid arteries, where it has been demonstrated to activate HIFs, and through the action of VEGF, stimulate intraplaque angiogenesis. Neovascularization has been associated with plaque growth, instability, and rupture and, therefore, hypoxia is implicated in the progression of atherosclerosis

### Influence of hypoxia on eNOS expression

> Various stimuli, such as oxidative stress, inflammation, and also hypoxia, affect eNOS expression
>
> eNOS uncoupling is considered the major cause of endothelial dysfunction observed in CVD. When eNOS itself becomes a source of superoxide and peroxynitrite anions, and NO is quenched, oxidative stress augments, leading to endothelial dysfunction and atherogenesis

### Hypoxia, Oxidative Stress and Endothelial Inflammation

> Oxidative stress is considered as the major contributor to eNOS uncoupling and endothelial dysfunction.
> Hypoxia not only reduces the bioavailability of nitric oxide directly but also acts indirectly by inducing oxidative stress.
> Hypoxia can induce endothelial cell activation, a proinflammatory and procoagulant state of ECs, characterized by the expression of cell–surface adhesion molecules
> ECs activation is driven by the `NF-κB` transcription factor, which is rapidly induced in response to a stimulus such as cytokines, bacterial or viral antigens, and stress signals, including hypoxia.
> Upon activation, `NF-κB` induces the expression of proinflammatory genes, mainly cytokines (TNF-α, IL-1, IL-2, IL-6, IL-8, IL-18) and adhesion molecules (VCAM-1, ICAM-1, E-selectin) that mediate leukocyte rolling, adhesion, and transendothelial migration, and initiate inflammatory cascade and atherogenesis
> Atherosclerosis is a chronic inflammatory state, initiated with endothelial cell activation. Endothelial activation, in turn, is associated with endothelial dysfunction since reduced NO bioavailability stimulates endothelial activation.
> Hypoxia elicits ECs activation and inflammation but, on the other hand, inflammatory diseases are frequently characterized by tissue hypoxia.
> The hypoxic response, oxidative stress, and inflammation are interrelated and overlapping mechanisms involved in the pathological reduction in NO synthesis and availability, leading to endothelial dysfunction

![Endothelial dysfunction](images/NO_endothelial_dysfunction.png)

## My takeaways

- This could explain episodes of hypoglycemia that I got post covid particularly on exertion. Basically to make energy, body requires oxygen. Insufficient oxygen delivery triggers the glycolysis pathway.

- In the long term, Can hypoxemia lead to excess VEGF-A from the endothelial cells which induces angiogenesis causing atherosclerosis?

- If the hypoxemia lasts long, then RBC and haemoglobin should increase. Also lactose dehydrogenase can be tested to check anaerobic metabolism.

- Less NO production means less vasodilation and more platelet adhesion to endothelium. This could affect the blood flow in the microcapillaries. **And remember BC007 was treated to improve the blood circulation in eyes! But it was related to the presence of GPCR-autoantibodies**

- Can pulmonary hypertension in covid patients be considered as a marker of hypoxemia?

- Intraplaque angiogenesis could be the reason why many post covid patients get heart attacks?

- Can supplements like L-arginine can help endothelial dysfunction due to lack of NO from hypoxemia.

---

## References

- [Endothelial Dysfunction Driven by Hypoxia—The Influence of Oxygen Deficiency on NO Bioavailability](https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC8301841/)
