# Epithelial and Endothelial Expressions of ACE2

---

## References

- [Epithelial and Endothelial Expressions of ACE2: SARS-CoV-2 Entry Routes](https://pubmed.ncbi.nlm.nih.gov/33626315/)
