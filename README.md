# Collection of Long Covid Resources

I am using this as my long covid diary. I am suffering from Long Covid and I am finding it hard to remember things that I read. So I started making notes to help my memory.

Long Covid being novel in nature, its hard to find much help on the diagnosis front from the Doctors. Its understandable since many of these studies are in nascent stage.

Since I am not a medical professional, I very often misunderstand medical terms, processes. And all the summaries, notes etc are all based on my understanding. I am just putting out resources that I read and gather and in that process I am trying to help myself in getting back my health.

I am planning to convert it to a blog and host it on [**netlify using Hugo**](https://www.kiroule.com/article/start-blogging-with-github-hugo-and-netlify/)
